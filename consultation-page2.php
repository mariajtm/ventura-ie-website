<?php

/**
* Template Name: Consultation Page New
* Selectable from a dropdown menu on the edit page screen. All images will be in lightbox
*/
?>

<?php 
get_header(); 
$mainUrl = get_bloginfo( 'template_directory' ) . '/';
?>


<?php
//  global $post;
  //  $address = get_post_meta( $post->ID, 'apslo_contact_address', true );

?>


<?php if (have_posts()) : ?>
 <?php while (have_posts()) : the_post(); ?>

<?php
$packages = get_post_meta( $post->ID, 'apslo_consultation_page_consultations', true );

$image_1 = get_post_meta( $post->ID, 'apslo_consultation_page_image_1', true );
$image_2 = get_post_meta( $post->ID, 'apslo_consultation_page_image_2', true );
?>


<div class="container consultation_container">



<div class="contaienr consultation_box_1">

<div class="col-sm-12 tagline" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="500">
    <h1><?php echo get_the_title();?></h1>
</div>
<div class="clear" style="height:0px"></div>

<div class="col-sm-12 consultation_box_1_text" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="300">
  <div class="pw_box_paragraph">
    <?php the_content();?>
  </div>  
</div>


<div class="clear" style="height:0px"></div>

<div class="col-sm-12 consultation_text_bottom">
<div class="col-sm-12 consultation_text_bottom_img consultation_text_bottom_img_l" paw-on-mobile="false" paw-animate="zoomIn" paw-delay="300">
    <img src="<?php echo $image_1;?>"/>
</div>
  
<div class="col-sm-12 consultation_text_bottom_img consultation_text_bottom_img_r" paw-on-mobile="false" paw-animate="zoomIn" paw-delay="300">
    <img src="<?php echo $image_2;?>"/>
</div>
</div>



</div><!-- consultation_box_1 -->


</div><!-- consultation_container -->

<div class="container-fluid consulting_options_holder">



<?php 
$rowCount = 0;
foreach( $packages as $cons){
  $cons['content'] = str_replace('[', '<b>', $cons['content']);
  $cons['content'] = str_replace(']', '</b>', $cons['content']);
?>

<div class="col-sm-12 single_consulting <?php if ($rowCount++ % 2 == 1 ) echo 'single_consulting_white'; ?>">
    <div class="col-sm-12 col-md-10 col-lg-6 col-sm-offset-0 col-md-offset-1 col-lg-offset-3  single_consulting_iner" paw-on-mobile="false" paw-animate="zoomIn" paw-delay="300">
        <h2><?php echo $cons['tagline'];?></h2>
        <p><?php echo nl2br($cons['content']);?></p>
        <div class="clear" style="height:20px"></div>
					<?php
					if($cons['link'] == 'd'){
					$link = get_site_url(). '/contact?d=1';
					}
          elseif($cons['link'] == 'architecture'){
            $link = get_site_url(). '/contact?a=1';
          }else{
					$link = $cons['link'];
					}

					?>
        <a href="<?php echo $link;?>" class="pw_button_ar_left"><?php echo $cons['link_text'];?></a>
    </div><!-- single_consulting_iner -->
</div><!-- single_consulting -->

<?php } ?>




</div><!-- consulting_options_holder -->


<?php endwhile; ?>
<?php else : ?>
  <div class="container consulting_options_holder">
		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>
  </div>
<?php endif; ?>


<div class="clear" style="height:0px"></div>






<?php get_footer(); ?>


</body>
</html>