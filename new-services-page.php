<?php

/**
* Template Name: New Services Page
* Selectable from a dropdown menu on the edit page screen. All images will be in lightbox
*/
?>

<?php 
get_header(); 
$mainUrl = get_bloginfo( 'template_directory' ) . '/';
?>


<?php
//  global $post;
  //  $address = get_post_meta( $post->ID, 'apslo_contact_address', true );

?>


<?php if (have_posts()) : ?>
 <?php while (have_posts()) : the_post(); ?>

<?php
$packages = get_post_meta( $post->ID, 'apslo_consultation_page_consultations', true );

$image_1 = get_post_meta( $post->ID, 'apslo_consultation_page_image_1', true );
$image_2 = get_post_meta( $post->ID, 'apslo_consultation_page_image_2', true );
?>


<div class="container consultation_container">



<div class="contaienr consultation_box_1">

<div class="col-sm-12 tagline" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="500">
    <h1><?php echo get_the_title();?></h1>
</div>
<div class="clear" style="height:0px"></div>

<div class="col-sm-12 consultation_box_1_text" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="300">
  <div class="pw_box_paragraph">
    <?php the_content();?>
  </div>  
</div>


<div class="clear" style="height:20px"></div>
<!-- 
<div class="col-sm-12 consultation_text_bottom">
<div class="col-sm-6 consultation_text_bottom_img consultation_text_bottom_img_l" paw-on-mobile="false" paw-animate="zoomIn" paw-delay="300">
    <img src="<?php echo $image_1;?>"/>
</div>
<div class="col-sm-6 consultation_text_bottom_img consultation_text_bottom_img_r" paw-on-mobile="false" paw-animate="zoomIn" paw-delay="300">
    <img src="<?php echo $image_2;?>"/>
</div>
</div> -->



</div><!-- consultation_box_1 -->


</div><!-- consultation_container -->

<div class="container-fluid consulting_options_holder">



<?php 
foreach( $packages as $cons){
?>

<div class="col-sm-12 single_consulting" paw-on-mobile="false" paw-animate="zoomIn" paw-delay="300" style="position:relative;">
  <div class="col-lg-6 service-img ">
    <img src="<?php echo $cons['image']?>" alt="" class="h-100">

  </div>
  <div class="col-lg-6  single_consulting_iner">
      <h2><?php echo $cons['tagline'];?></h2>
      <p style="font-size:14px ;"><?php echo nl2br($cons['content']);?></p>
      <div class="clear" style="height:20px"></div>
        <?php
        if($cons['link'] == 'concept2completion'){
        $link = get_site_url(). '/contact?concept2completion=1';
        }
        elseif($cons['link'] == 'HomeDesign'){
          $link = get_site_url(). '/contact?HomeDesign=1';
        }
        else{
        $link = $cons['link'];
        }

        ?>
      <!-- <?php if ($cons['link'] == 'HomeDesign') : ?>
        <a href="https://ventura.ie/_assets/Ventura-Brochure.pdf" target="_blank" class="btn-primary btn-lg">READ MORE</a>
        <div class="clear" style="height:35px"></div>
      <?php endif; ?> -->
      <a href="<?php echo $link;?>" target="_blank" class="btn-primary btn-lg"><?php echo $cons['link_text'];?></a>

  </div>
</div><!-- single_consulting -->
<div class="clear" style="height: 50px;"></div>

<?php } ?>




</div><!-- consulting_options_holder -->


<?php endwhile; ?>
<?php else : ?>
  <div class="container consulting_options_holder">
		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>
  </div>
<?php endif; ?>


<div class="clear" style="height:20px"></div>






<?php get_footer(); ?>


</body>
</html>