<?php get_header(); ?>

<style>
  .single_portfolio_container{
    border:none;
  }
  .single_portfolio_title{
    text-align:center;
  }
  .single_portfolio_content{
    text-align:center;
  }
  .showroom_container {
  position: relative;
  text-align: center;
}

.showroom_gallery {
  height: 200px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  border: 3px solid #F6F6F6;
}

.showroom_gallery_2 {
  height: 150px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  border: 3px solid #F6F6F6;
}
.showroom_details h3{
    font-family: 'Playfair display';
  
  
}
@media (max-width:997px) {
  .showroom_details{
    text-align: center;
  }
  
}
</style>


<div class="clear mobile_hidden" style="height:150px;"></div>
  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
  <div class="container showroom_container mobile_hidden" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="0">
    <h2><?php echo get_the_title() ?></h2>
    <?php
      $desc = get_post_meta( $post->ID, 'mt_showroom_description', true );
      $address = get_post_meta( $post->ID, 'mt_showroom_address', true );
      $hours = get_post_meta( $post->ID, 'mt_showroom_hours', true );
      $contact = get_post_meta( $post->ID, 'mt_showroom_contact', true );
      $featured_img = get_the_post_thumbnail_url();
    ?>
    <p><?php  echo nl2br($desc);?></p> 
    <div class="clear" style="height: 20px;"></div>
    <a href="<?php echo get_site_url() ?>/appointments/"  class="btn btn-primary"> BOOK HERE </a>

  </div> 
<div class="clear" style="height:20px"></div>
<div class="container">
  <div class="row">

  
  <div class="col-md-4 showroom_details">

    <div class="row">
      <h3>LOCATION</h3>
      <p><?php echo nl2br($address)?></p>
    </div>
    <div class="row">
      <h3>OPENING HOURS</h3>
      <p><?php echo nl2br($hours)?></p>
    </div>
    <div class="row">
      <h3>CONTACT</h3>
      <p><?php echo nl2br($contact)?></p>
    </div>
  </div>
  <div class="col-md-8">
    <img src="<?php echo $featured_img?>" style="width: 100%!important;">
  </div>
  </div>

</div>
<div class="clear" style="height:80px;"></div>

<div class="container showroom_container mobile_hidden" >
  <div class="row" style="padding: 50px 30px; background: #F6F6F6;" >
 
  <?php

    
    $content = get_the_content();
    $origImageSrc = array();

    preg_match_all('/<img[^>]+>/i',$content, $imgTags); 

    for ($i = 0; $i < count($imgTags[0]); $i++) {
      preg_match('/src="([^"]+)/i',$imgTags[0][$i], $imgage);
      // remove opening 'src=' tag, can`t get the regex right
      $origImageSrc[] = str_ireplace( 'src="', '',  $imgage[0]);
    }

  
  ?>
  <style>
 .fancybox-caption{
    text-align:center;
       border-top: 1px solid hsla(0,0%,100%,.4);
  }
</style>



  <?php
  foreach( $origImageSrc as $image){
      if($origImageSrc[0] != $image){
          echo '
          <a href="'. $image .'" data-fancybox="group_'. $post->ID.'" data-caption="" data-type="image">
          <div class="col-sm-3 showroom_gallery" style="background-image: url('.$image.')" ></div> 
          
          </a>      
      ';
      }
  }
  ?>
  </div>

 
<!-- portfolio_item_out -->
</div><!-- gallery -->



 <div class="clear" style="height:80px;"></div>



<?php endwhile; ?>
<?php else : ?>
  <div class="container single_portfolio_content">
		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>
  </div>
<?php endif; ?>

  </div><!-- portfolio_container -->


<div class="clear" style="height:10px;"></div>


<?php get_footer(); ?>


</body>
</html>