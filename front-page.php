<?php

/**
* Template Name: Front Page
* Selectable from a dropdown menu on the edit page screen. All images will be in lightbox
*/
?>


<?php get_header(); ?>
<!--
<?php
global $post;
echo "pageid: ".$post->ID;
?>
-->

<?php
global $post;

$tagline_1 = get_post_meta( $post->ID, 'pw_front_tagline_1', true );
$portfolio_link = get_post_meta( $post->ID, 'pw_front_portfolio_link', true );
$profile_image = get_post_meta( $post->ID, 'pw_front_profile_image', true );
$profile_text = get_post_meta( $post->ID, 'pw_front_profile_text', true );
$tagline_2 = get_post_meta( $post->ID, 'pw_front_tagline_2', true );

$video_boxes = get_post_meta( $post->ID, 'pw_front_videos_', true );

?>
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->



<?php  include('partials/video.php');?>
<div class="clear"></div>

<?php include('partials/home_1.php');?>
<div class="clear"></div>

<?php include('partials/home_2.php');?>
<div class="clear"></div>

<?php include('partials/home_3.php');?>
<div class="clear"></div>

<?php include('partials/home_4.php');?>
<div class="clear"></div>



<div class="clear" style="height:0px;"></div>


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->



<!-- showrooms
<style>
.showrooms_box {
position: fixed;
top: 15%;
right: -100px;
width: 140px;
padding-left: 15px;
height: 40px;
background-color: #958D8A;
border-radius: 6px 0px 0px 6px;
display: flex;
align-items: center;
justify-content: left;
color: #fff;
transition: all 0.5s ease; }
.showrooms_box i {
font-size: 25px;
margin-right: 12px; }
.showrooms_box span {
font-size: 12px; }

.showrooms_box:hover {
right: 0px; }
</style>
<a href="#showrooms_box" class="page-scroll">
<div class="showrooms_box showrooms_box_hide">
<i class="fa fa-map-marker" aria-hidden="true"></i> <span>Showrooms</span>
</div>
</a>
 //showrooms -->



<?php get_footer(); ?>



<style>
  .insta_th_out{
    padding:2px;
    margin:0px;
  }
  .insta_th{
    padding:0px;
    margin:0px;
  }
  
</style>

<!--
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/instafeed.min.js"></script>
-->
<script type="text/javascript">
/*
var template = '<a href="{{link}}" target="_blank"><div class="col-xs-12 col-sm-4 col-md-4 insta_th_out"><div class="col-xs-12 insta_th" style="background-image:url(\'{{image}}\');"><div class="insta_cover"><p>{{model.short_caption}}</p></div></div></div></a>';


  var userFeed = new Instafeed({
    get: 'user',
    userId: '2178877574',
    clientId: 'ab4652288bf74085b2dedb7420736e20',
   // accessToken: '2178877574.ab46522.ab10ef70fbb441f4ae751dfb3cf69b28',
     accessToken: '2178877574.ab46522.9f0bf1b385d049e987ee1ad4f193158b', 
    resolution: 'standard_resolution',
  //  template: '<a href="{{link}}" target="_blank" id="{{id}}"><img src="{{image}}" /></a>',
  filter: function(image) {
      var MAX_LENGTH = 100;

      // here we create a property called "short_caption"
      // on the image object, using the original caption
      if (image.caption && image.caption.text) {
        image.short_caption = image.caption.text.slice(0, MAX_LENGTH) + '...';
      } else {
        image.short_caption = "";
      }

      // ensure the filter doesn't reject any images
      return true;
    },
  template: template,
    sortBy: 'most-recent',
    limit: 12,
    links: false,
    before: instaUpdateBefore('Loading...'),
    after: function(){ $('.insta_th').css('height', $('.insta_th').width()); },
    success: instaUpdateAfter()
  });
  userFeed.run();
  


function instaUpdateBefore(mess){
    $('#instafeed_mess').html('<p>'+mess+'</p>');
}

function instaUpdateAfter(){
    $('#instafeed_mess').html('');
    //substring(0, 10)

    
}
*/

</script>


</body>
</html>
