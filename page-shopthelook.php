<?php

/**
* Template Name: New Shop The Look Page
* Selectable from a dropdown menu on the edit page screen. All images will be in lightbox
*/
?>


<?php get_header(); ?>




<div class="clear" style="height:0px"></div>

<div class="container portfolio_container">

<div class="col-sm-12 tagline" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="500">
    <h1>Shop The</h1>
</div>
<div class="clear" style="height:0px"></div>



<div class="col-sm-12">


<?php
$mainUrl = get_template_directory_uri() . '/';


$type = 'portfolio';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
);

$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post();
  $format = get_post_format( $post->ID );
  
    $desc = get_post_meta( $post->ID, 'pw_portfolio_gallery_box_desc', true );
    $poster= get_post_meta( $post->ID, 'pw_portfolio_gallery_image', true );
    $content = get_the_content();

  
  
 ?> 
  
  <div class="col-sm-6 portfolio_item_out">
    <div class="col-sm-12 portfolio_item_in">
<a href="<?php the_permalink();?>">   
        <div class="col-sm-12 portfolio_item_poster" style="background-image:url('<?php echo $poster;?>');">
        <img class="portfolio__poster" src="<?php echo get_template_directory_uri();?>/img/default_portfolio.jpg"/>
        <div class="cover"></div>
        <div class="overlay">
         <!-- <img src="<?php echo get_template_directory_uri();?>/img/gallery.png"/> -->
          </div>
        </div>
      </a>
        <div class="col-sm-12 portfolio_item_desc">
            <p> 
                <b><?php echo get_the_title();?></b>
              <span style="padding-left:10px;">
                <a href="<?php the_permalink();?>" style="color:#000;">read more...</a>
              </span>
            </p>
        </div>
    </div>
</div>

  
  
  
<!-- end new -->
  
<?php 
 endwhile;
}
?>








</div><!-- portfolio_holder -->





</div><!-- portfolio_container -->


<div class="clear" style="height:120px"></div>









<?php get_footer(); ?>










</body>
</html>


