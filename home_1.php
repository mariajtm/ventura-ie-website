<div id="section_1" class="container section_1_container">

<div class="col-sm-12 tagline" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="100">
    <h1><?php echo $tagline_1;?></h1>
</div>

<div class="clear" style="height:0px"></div>

<div class="col-sm-12 portfolio_holder">





<?php
wp_reset_query();
$args = array( 'post_type' => 'portfolio', 'post_status' => 'publish', 'posts_per_page' => 4 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
//$test_content = get_post_meta( $post->ID, 'apslo_testimonials_content', true );
$format = get_post_format( $post->ID );

if($format == 'video'){
    $desc = get_post_meta( $post->ID, 'pw_portfolio_video_box_desc', true );
    $video = get_post_meta( $post->ID, 'pw_portfolio_video_video', true );
    $poster= get_post_meta( $post->ID, 'pw_portfolio_video_image', true );
  //  include('loop_video.php');
  }else{
    $desc = get_post_meta( $post->ID, 'pw_portfolio_gallery_box_desc', true );
    $poster= get_post_meta( $post->ID, 'pw_portfolio_gallery_image', true );
    $content = get_the_content();

  //  include('loop_gallery_new.php');
  }
?>
  
  <!-- new -->
  <!--
  <a href="<?php the_permalink();?>">
<div class="col-sm-6 front_portfolio_out">
    <div class="col-sm-12 front_portfolio_in">
        <div class="portfolio_cover" style="background-image:url('<?php echo get_the_post_thumbnail_url();?>')">
            <img src="<?php echo get_the_post_thumbnail_url();?>"/>
            <div class="cover"></div>
        </div>

    <div class="clear" style="height:0px"></div>
    <h3><?php echo get_the_title();?></h3>
    </div>
</div>
</a>
-->
  
  <div class="col-sm-6 portfolio_item_out" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="0">
    <div class="col-sm-12 portfolio_item_in">
<a href="<?php the_permalink();?>">   
        <div class="col-sm-12 portfolio_item_poster" style="background-image:url('<?php echo $poster;?>');">
        <img class="portfolio__poster" src="<?php echo get_template_directory_uri();?>/img/default_portfolio.jpg"/>
        <div class="cover"></div>
        <div class="overlay">
         <!-- <img src="<?php echo get_template_directory_uri();?>/img/gallery.png"/> -->
          </div>
        </div>
      
        <div class="col-sm-12 portfolio_item_desc">
            <p> 
                <b><?php echo get_the_title();?></b>
              
            </p>
        </div>
        </a>
    </div>
</div>
  
  
  
<!-- end new -->

<?php endwhile; ?>
<?php  wp_reset_query(); ?>






<div class="clear" style="height:0px"></div>

<div class="col-sm-12 portfolio_button_holder" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="300">
    <a href="<?php echo $portfolio_link;?>" class="pw_button_ar_left">VIEW OUR GALLERY</a>
</div>


</div><!-- portfolio_holder -->


</div><!-- section_1_container -->