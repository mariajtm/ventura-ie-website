<?php

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
	
}
elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
	
}


function apslo_show_if_front_page( $cmb ) {
	
	// 	Don't show this metabox if it's not the front page template
	// 	usage: 'show_on_cb' => 'yourprefix_show_if_front_page',
	if ( $cmb->object_id !== get_option( 'page_on_front' ) ) {
		
		return false;
		
	}
	
	return true;
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


add_action( 'cmb2_admin_init', 'pw_fc_front_page_settings' );

function pw_fc_front_page_settings(){
	$prefix = 'pw_front_';


	$cmb_front_page = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_front_page',
		'title'         => __( 'Front Page Settings', 'cmb2' ),
		'object_types'  => array( 'page'), // 	Post type
		'on_front'        => true,
		//'show_on_cb' => 'apslo_show_if_front_page',
    'show_on' => array( 'key' => 'page-template', 'value' => 'front-page.php' ),
		) );


		$cmb_front_page->add_field( array(
			'name'       => __( 'Tagline 1', 'cmb2' ),
			'id'         => $prefix . 'tagline_1',
			'description' => __( 'Text under video on home page', 'cmb2' ),
			'type' => 'textarea_small',
			) );


			$pages = get_pages();
			
			$apslo_pages_array = array();
			
			foreach ( $pages as $page ) {
				
				$apslo_pages_array[ get_page_link( $page->ID ) ] =  __( $page->post_title , 'cmb2' );
				
			}
			
			$cmb_front_page->add_field( array(
			'name'             => 'Portfolio Page',
			'desc'             => 'Select an portfolio page',
			'id'               => $prefix . 'portfolio_link',
			'type'             => 'select',
			'show_option_none' => true,
			'default'          => 'custom',
			'options'          => $apslo_pages_array,
		
			
			) );			



		$cmb_front_page->add_field( array(
			'name'       => __( 'Profile text', 'cmb2' ),
			'id'         => $prefix . 'profile_text',
			'type' => 'textarea',
			) );		
			
			
			$cmb_front_page->add_field( array(
				'name'       => __( 'Profile image', 'cmb2' ),
				'id'         => $prefix . 'profile_image',
				'type' => 'file',
				) );	


				$cmb_front_page->add_field( array(
					'name'       => __( 'Tagline 2', 'cmb2' ),
					'id'         => $prefix . 'tagline_2',
					'type' => 'textarea_small',
				) );		


	// 	$group_field_id is the field id string, so in this case: $prefix . 'service_'
	$group_field_1 = $cmb_front_page->add_field( array(
		'id'          => $prefix . 'videos_',
		'type'        => 'group',
		'description' => __( 'Generates videos displayed on front page', 'cmb2' ),
		'options'     => array(
		'group_title'   => __( 'Video boxes {#}', 'cmb2' ),
		'add_button'    => __( 'Add Another box', 'cmb2' ),
		'remove_button' => __( 'Remove box', 'cmb2' ),
		'sortable'      => true, // 	beta
		 	'closed'     => true, // 	true to have the groups closed by default
		),
		) );
		
		
		$cmb_front_page->add_group_field( $group_field_1, array(
		'name' => __( 'Poster', 'cmb2' ),
		'id'   => 'image',
		'type' => 'file',
		) );

		$cmb_front_page->add_group_field( $group_field_1, array(
		'name' => __( 'Video No', 'cmb2' ),
		'description' => __( 'Video number from Vimeo', 'cmb2' ),
		'id'   => 'video',
		'type' => 'text',
		) );

		$cmb_front_page->add_group_field( $group_field_1, array(
		'name' => __( 'Video description', 'cmb2' ),
		'id'   => 'box_desc',
		'type' => 'text',
		) );


//added
		$cmb_front_page->add_group_field( $group_field_1, array(
		'name' => __( 'Gallery image 1', 'cmb2' ),
		'id'   => 'image_gal_1',
		'type' => 'file',
		) );

		$cmb_front_page->add_group_field( $group_field_1, array(
		'name' => __( 'Gallery image 2', 'cmb2' ),
		'id'   => 'image_gal_2',
		'type' => 'file',
		) );
	
			$cmb_front_page->add_group_field( $group_field_1, array(
		'name' => __( 'Gallery image 3', 'cmb2' ),
		'id'   => 'image_gal_3',
		'type' => 'file',
		) );


		$cmb_front_page->add_group_field( $group_field_1, array(
		'name' => __( 'Box number', 'cmb2' ),
		'id'   => 'no',
		'type' => 'text_small',
		) );




}//pw_fc_front_page_settings


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





//================== section portfolio ================

//NEW PROJECT DESCRIPTION FIELD
add_action( 'cmb2_admin_init', 'mt_fc_single_portfolio_descr' );


function mt_fc_single_portfolio_descr() {
	
	$prefix = 'mt_portfolio_';
	
	$cmb_portfolio_desc = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_portfolio_box',
	'title'         => __( 'Portfolio Description', 'cmb2' ),
	'object_types'  => array( 'portfolio'), // 	Post type
	'on_front'        => true,
	) );

	$cmb_portfolio_desc->add_field( array(
		'name' => __( 'Description', 'cmb2' ),
		'id'   => $prefix . 'main_desc',
		'type' => 'textarea',
	) );
	
	
} //PROJECT DESCRIPTION

// mt_portfolio_gallery
add_action( 'cmb2_admin_init', 'mt_portfolio_gallery' );


function mt_portfolio_gallery() {
	
	$prefix = 'mt_portfolio_rooms';
	
	$cmb_portfolio_products_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_portfolio_box',
	'title'         => __( 'Product settings', 'cmb2' ),
	'object_types'  => array( 'portfolio'), // 	Post type
	'on_front'        => true,
	) );

	$cmb_portfolio_products_page->add_field( array(
		'name' => __( 'Add any products related to this project', 'cmb2' ),
		'id'   => $prefix . 'title',
		'type' => 'title',
	) );

	//group
	// 	$group_field_id is the field id string, so in this case: $prefix . 'related_products'
	$cons_group_field_id = $cmb_portfolio_products_page->add_field( array(
		'id'          => $prefix . 'related_products',
		'type'        => 'group',
		'options'     => array(
		'group_title'   => __( 'Product {#}', 'cmb2' ),
		'add_button'    => __( 'Add Another Product', 'cmb2' ),
		'remove_button' => __( 'Remove Product', 'cmb2' ),
		'sortable'      => true, // 	beta
		 'closed'     => true, // 	true to have the groups closed by default
		),
	) );
		
	
	$cmb_portfolio_products_page->add_group_field( $cons_group_field_id, array(
		'name' => __( 'Name', 'cmb2' ),
		'id'   => 'name',
		'type' => 'text',
	) );

	$cmb_portfolio_products_page->add_group_field( $cons_group_field_id, array(
		'name' => __( 'Image', 'cmb2' ),
		'id'   => 'image',
		'type' => 'file',
		) );	
	
	$cmb_portfolio_products_page->add_group_field( $cons_group_field_id, array(
		'name' => __( 'Link to page', 'cmb2' ),
		'id'   => 'link',
		'type' => 'text',
	) );	


	//end group
	
}
// mt_portfolio_gallery





//================== end section portfolio =============
//==============section showrooms==========//


add_action( 'cmb2_admin_init', 'mt_fc_single_showroom' );


function mt_fc_single_showroom() {
	
	$prefix = 'mt_showroom_';
	
	$cmb_portfolio_video_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_showroom_box',
	'title'         => __( 'Showroom Settings', 'cmb2' ),
	'object_types'  => array( 'showroom',), // 	Post type
	'on_front'        => true,
	) );
	

	$cmb_portfolio_video_page->add_field( array(
		'name' => __( 'Description', 'cmb2' ),
		'id'   => $prefix . 'description',
		'type' => 'textarea',
	) );
	$cmb_portfolio_video_page->add_field( array(
		'name' => __( 'Address', 'cmb2' ),
		'id'   => $prefix . 'address',
		'type' => 'textarea',
	) );
	$cmb_portfolio_video_page->add_field( array(
		'name' => __( 'Opening Hours', 'cmb2' ),
		'id'   => $prefix . 'hours',
		'type' => 'textarea',
	) );
	$cmb_portfolio_video_page->add_field( array(
		'name' => __( 'Contact Details', 'cmb2' ),
		'id'   => $prefix . 'contact',
		'type' => 'textarea',
	) );
  
	
	
}
//================== end section showrooms =============


//================== section product ================
add_action( 'cmb2_admin_init', 'apslo_register_product' );


function apslo_register_product() {
	
	$prefix = 'apslo_product_';
	
	
	
	$cmb_links_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_links_box',
	'title'         => __( 'Product data', 'cmb2' ),
	'object_types'  => array( 'product'), // 	Post type
	'on_front'        => true,
	) );
	
	
	$cmb_links_page->add_field( array(
	'name'       => __( 'Description', 'cmb2' ),
	'id'         => $prefix . 'desc',
	'type' => 'textarea',
	) );
	
	
	
	$cmb_links_page->add_field( array(
	'name'       => __( 'Spec Sheet', 'cmb2' ),
	'description' => __( 'Link to product spec sheet', 'cmb2' ),
	'id'         => $prefix . 'spec',
	'type' => 'text',
	) );
	
		
	$cmb_links_page->add_field( array(
	'name'       => __( 'Thumbnail replacment', 'cmb2' ),
	'description' => __( 'Thumbnail replacment', 'cmb2' ),
	'id'         => $prefix . 'th_replace',
	'type' => 'text',
	) );
	
	
	
}
// apslo_register_product

//================== end section product =============




//================== section product page ================

add_action( 'cmb2_admin_init', 'apslo_register_product_page' );


function apslo_register_product_page() {
	
	$prefix = 'apslo_product_page_';
	
	
	
	$cmb_links_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_links_box',
	'title'         => __( 'Products page additional settings', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => 'products-page.php' ),
	) );
	
	
	
	$cmb_links_page->add_field( array(
	'name'       => __( 'Bottom tagline', 'cmb2' ),
	'id'         => $prefix . 'text',
	'type' => 'text',
	) );
	
	
	
	
	
}
// apslo_register_product_page


//================== end section product page =============



//================== section services page ================

add_action( 'cmb2_admin_init', 'apslo_register_services_page' );


function apslo_register_services_page() {
	
	$prefix = 'apslo_services_page_';


	$pages = get_pages();
	
	$apslo_pages_array = array();
	
	foreach ( $pages as $page ) {
		
		$apslo_pages_array[ get_page_link( $page->ID ) ] =  __( $page->post_title , 'cmb2' );
		
	}
	
	
	
	$cmb_services_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_links_box',
	'title'         => __( 'Services page additional settings', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => 'services-page.php' ),
	) );
	
	
	
	//=	==========================
	$cmb_services_page->add_field( array(
	'name'       => __( 'Vimeo settings', 'cmb2' ),
	'id'         => $prefix . 'vimeo_title',
	'type' => 'title',
	) );
	
	
	
	$cmb_services_page->add_field( array(
	'name'       => __( 'Vimeo video No.', 'cmb2' ),
	'id'         => $prefix . 'vimeo',
	'type' => 'text',
	) );
	
	
	$cmb_services_page->add_field( array(
	'name'       => __( 'Vimeo video text', 'cmb2' ),
	'id'         => $prefix . 'text',
	'type' => 'text',
	) );
	
	
	$cmb_services_page->add_field( array(
	'name'       => __( 'Vimeo poster', 'cmb2' ),
	'id'         => $prefix . 'poster',
	'type' => 'file',
	) );
	
	
	
	//=	==========================
	$cmb_services_page->add_field( array(
	'name'       => __( 'We part settings', 'cmb2' ),
	'id'         => $prefix . 'we_title',
	'type' => 'title',
	) );
	
	
	//group
	// 	$group_field_id is the field id string, so in this case: $prefix . 'service_'
	$services_group_field_id = $cmb_services_page->add_field( array(
		'id'          => $prefix . 'we_',
		'type'        => 'group',
		'description' => __( 'Generates We boxes displayed on services page', 'cmb2' ),
		'options'     => array(
		'group_title'   => __( 'Service {#}', 'cmb2' ),
		'add_button'    => __( 'Add Another Service', 'cmb2' ),
		'remove_button' => __( 'Remove Service', 'cmb2' ),
		'sortable'      => true, // 	beta
		 	'closed'     => true, // 	true to have the groups closed by default
		),
		) );
		
		
		$cmb_services_page->add_group_field( $services_group_field_id, array(
		'name' => __( 'Tagline', 'cmb2' ),
		'id'   => 'tagline',
		'type' => 'text',
		) );

		$cmb_services_page->add_group_field( $services_group_field_id, array(
			'name' => __( 'Text', 'cmb2' ),
			'id'   => 'content',
			'type' => 'textarea_small',
			) );	
			
			$cmb_services_page->add_group_field( $services_group_field_id, array(
				'name' => __( 'Link to page', 'cmb2' ),
				'id'   => 'link',
				'type'             => 'select',
				'show_option_none' => true,
			//	'default'          => 'custom',
				'options'          => $apslo_pages_array,
				) );	
	
			$cmb_services_page->add_group_field( $services_group_field_id, array(
		'name' => __( 'Button text', 'cmb2' ),
		'id'   => 'link_text',
		'type' => 'text',
		) );

	//end group
	
	//=	==========================
	$cmb_services_page->add_field( array(
		'name'       => __( 'Bottom images', 'cmb2' ),
		'id'         => $prefix . 'we_bottom_img',
		'type' => 'title',
	) );	
	
	$cmb_services_page->add_field( array(
		'name'       => __( 'Bottom image left', 'cmb2' ),
		'id'         => $prefix . 'btm_img_1',
		'type' => 'file',
	) );	


	$cmb_services_page->add_field( array(
		'name'       => __( 'Bottom image right', 'cmb2' ),
		'id'         => $prefix . 'btm_img_2',
		'type' => 'file',
	) );
	
	
	
	
		$cmb_services_page->add_field( array(
		'name'       => __( 'Bottom tagline', 'cmb2' ),
		'id'         => $prefix . 'btm_tagline',
		'type' => 'text',
	) );
	
	
		$cmb_services_page->add_field( array(
		'name'       => __( 'Bottom text', 'cmb2' ),
		'id'         => $prefix . 'btm_text',
		'type' => 'textarea',
	) );

	
}
// apslo_register_services_page

//================== end section services page =============


//================== section consultation page ================

add_action( 'cmb2_admin_init', 'apslo_register_consultation_page' );


function apslo_register_consultation_page() {
	
	$prefix = 'apslo_consultation_page_';
	
	
	$cmb_consultation_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_consultation_box',
	'title'         => __( 'Consultation page additional settings', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => array( 'consultation-page.php', 'consultation-page2.php','new-services-page.php') ),
	) );
	
	
	$cmb_consultation_page->add_field( array(
	'name'       => __( 'Image 1', 'cmb2' ),
	'id'         => $prefix . 'image_1',
	'type' => 'file',
	) );
	
		$cmb_consultation_page->add_field( array(
	'name'       => __( 'Image 2', 'cmb2' ),
	'id'         => $prefix . 'image_2',
	'type' => 'file',
	) );
	
	

		//group
	// 	$group_field_id is the field id string, so in this case: $prefix . 'consultations'
	$cons_group_field_id = $cmb_consultation_page->add_field( array(
		'id'          => $prefix . 'consultations',
		'type'        => 'group',
		'description' => __( 'Generates Consultation packages boxes displayed on consultation page', 'cmb2' ),
		'options'     => array(
		'group_title'   => __( 'Package {#}', 'cmb2' ),
		'add_button'    => __( 'Add Another Package', 'cmb2' ),
		'remove_button' => __( 'Remove Package', 'cmb2' ),
		'sortable'      => true, // 	beta
		 'closed'     => true, // 	true to have the groups closed by default
		),
		) );
		
		
		$cmb_consultation_page->add_group_field( $cons_group_field_id, array(
		'name' => __( 'Name', 'cmb2' ),
		'id'   => 'tagline',
		'type' => 'text',
		) );

		$cmb_consultation_page->add_group_field( $cons_group_field_id, array(
			'name' => __( 'Text', 'cmb2' ),
			'id'   => 'content',
			'type' => 'textarea_small',
			) );	
			
		$cmb_consultation_page->add_group_field( $cons_group_field_id, array(
			'name' => __( 'Link to page', 'cmb2' ),
			'id'   => 'link',
			'type' => 'text',
			) );	
	
		$cmb_consultation_page->add_group_field( $cons_group_field_id, array(
			'name' => __( 'Button text', 'cmb2' ),
			'id'   => 'link_text',
			'type' => 'text',
		) );
		$cmb_consultation_page->add_group_field( $cons_group_field_id, array(
			'name' => __( 'Image', 'cmb2' ),
			'id'   => 'image',
			'type' => 'file',
		) );

	//end group
	
	
	
	
}
// apslo_register_consultation_page

//================== end section consultation page =============






//================== section workshop page ================

add_action( 'cmb2_admin_init', 'apslo_register_workshop_page' );


function apslo_register_workshop_page() {
	
	$prefix = 'apslo_workshop_page_';
	
	
	$cmb_workshop_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_workshop_box',
	'title'         => __( 'Workshop page additional settings', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => 'workshop-page.php' ),
	) );
	
	
	
			//group
	$workshop_group_field_id = $cmb_workshop_page->add_field( array(
		'id'          => $prefix . 'videos',
		'type'        => 'group',
		'description' => __( 'Generates Videos  boxes displayed on workshop page', 'cmb2' ),
		'options'     => array(
		'group_title'   => __( 'Video {#}', 'cmb2' ),
		'add_button'    => __( 'Add Another Video', 'cmb2' ),
		'remove_button' => __( 'Remove Video', 'cmb2' ),
		'sortable'      => true, // 	beta
		 'closed'     => true, // 	true to have the groups closed by default
		),
		) );
		
	

		$cmb_workshop_page->add_group_field( $workshop_group_field_id, array(
			'name' => __( 'Vimeo video poster', 'cmb2' ),
			'id'   => 'poster',
			'type' => 'file',
			) );	
			
			$cmb_workshop_page->add_group_field( $workshop_group_field_id, array(
				'name' => __( 'Vimeo video No.', 'cmb2' ),
				'id'   => 'vimeo',
				'type' => 'text',
				) );	
	
			$cmb_workshop_page->add_group_field( $workshop_group_field_id, array(
		'name' => __( 'Vimeo video text', 'cmb2' ),
		'id'   => 'text_desc',
		'type' => 'text',
		) );

	//end group
	
	
	
}
// apslo_register_workshop_page

//================== end section workshop page =============

//================@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//================== section workshop page ================

add_action( 'cmb2_admin_init', 'apslo_register_workshop_page2' );


function apslo_register_workshop_page2() {
	
	$prefix = 'apslo_workshop_page2_';
	
	
	$cmb_workshop_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_workshop_box2',
	'title'         => __( 'Video settings', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => 'services-page.php' ),
	) );
	
	
	
			//group
	$workshop_group_field_id = $cmb_workshop_page->add_field( array(
		'id'          => $prefix . 'videos',
		'type'        => 'group',
		'description' => __( 'Generates Videos  boxes displayed on workshop page', 'cmb2' ),
		'options'     => array(
		'group_title'   => __( 'Video {#}', 'cmb2' ),
		'add_button'    => __( 'Add Another Video', 'cmb2' ),
		'remove_button' => __( 'Remove Video', 'cmb2' ),
		'sortable'      => true, // 	beta
		 'closed'     => true, // 	true to have the groups closed by default
		),
		) );
		
	

		$cmb_workshop_page->add_group_field( $workshop_group_field_id, array(
			'name' => __( 'Vimeo video poster', 'cmb2' ),
			'id'   => 'poster',
			'type' => 'file',
			) );	
			
			$cmb_workshop_page->add_group_field( $workshop_group_field_id, array(
				'name' => __( 'Vimeo video No.', 'cmb2' ),
				'id'   => 'vimeo',
				'type' => 'text',
				) );	
	
			$cmb_workshop_page->add_group_field( $workshop_group_field_id, array(
		'name' => __( 'Vimeo video text', 'cmb2' ),
		'id'   => 'text_desc',
		'type' => 'text',
		) );

	//end group
	
	
	
}
// apslo_register_workshop_page2

//================== end section workshop page2 =============
//=================@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


//============== section contact ====================
// show only on page template caled portfolio
add_action( 'cmb2_admin_init', 'apslo_contact_page_metabox' );


function apslo_contact_page_metabox() {
	
	$prefix = 'apslo_contact_';
	
	
	$cmb_contact_page = new_cmb2_box( array(
	'id'           => $prefix . 'metabox_portfolio',
	'title'        => __( 'Contact Page', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => 'contact.php' ),
	) );
	
	
	
	//address 1
			$cmb_contact_page->add_field( array(
	'name'       => __( 'Address 1 tagline ', 'cmb2' ),
	'id'         => $prefix . 'tagline_1',
	'type'       => 'text',
	) );
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Address 1', 'cmb2' ),
	'id'         => $prefix . 'address_1',
	'type'       => 'textarea_small',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Telephone 1', 'cmb2' ),
	'id'         => $prefix . 'tel_1',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Telephone link 1', 'cmb2' ),
	'id'         => $prefix . 'tel_link_1',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Email 1', 'cmb2' ),
	'id'         => $prefix . 'email_1',
	'type'       => 'text',
	) );
	
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Latitude 1', 'cmb2' ),
	'id'         => $prefix . 'lat_1',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Longitude 1', 'cmb2' ),
	'id'         => $prefix . 'long_1',
	'type'       => 'text',
	) );
	
	
		//address 2
		$cmb_contact_page->add_field( array(
	'name'       => __( 'Address 2 tagline ', 'cmb2' ),
	'id'         => $prefix . 'tagline_2',
	'type'       => 'text',
	) );	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Address 2', 'cmb2' ),
	'id'         => $prefix . 'address_2',
	'type'       => 'textarea_small',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Telephone 2', 'cmb2' ),
	'id'         => $prefix . 'tel_2',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Telephone link 2', 'cmb2' ),
	'id'         => $prefix . 'tel_link_2',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Email 2', 'cmb2' ),
	'id'         => $prefix . 'email_2',
	'type'       => 'text',
	) );
	
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Latitude 2', 'cmb2' ),
	'id'         => $prefix . 'lat_2',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Longitude 2', 'cmb2' ),
	'id'         => $prefix . 'long_2',
	'type'       => 'text',
	) );
	
	
	
			//address 3
		$cmb_contact_page->add_field( array(
	'name'       => __( 'Address 3 tagline ', 'cmb2' ),
	'id'         => $prefix . 'tagline_3',
	'type'       => 'text',
	) );
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Address 3', 'cmb2' ),
	'id'         => $prefix . 'address_3',
	'type'       => 'textarea_small',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Telephone 3', 'cmb2' ),
	'id'         => $prefix . 'tel_3',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Telephone link 3', 'cmb2' ),
	'id'         => $prefix . 'tel_link_3',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Email 3', 'cmb2' ),
	'id'         => $prefix . 'email_3',
	'type'       => 'text',
	) );
	
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Latitude 3', 'cmb2' ),
	'id'         => $prefix . 'lat_3',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page->add_field( array(
	'name'       => __( 'Longitude 3', 'cmb2' ),
	'id'         => $prefix . 'long_3',
	'type'       => 'text',
	) );
	
	
	

	
	
}
//apslo_contact_page_metabox

//============== end section contact ===============

//============== section contact UK ====================
// show only on page template caled portfolio
add_action( 'cmb2_admin_init', 'apslo_contact_uk_page_metabox' );


function apslo_contact_uk_page_metabox() {
	
	$prefix = 'apslo_contact_uk_';
	
	
	$cmb_contact_page_uk = new_cmb2_box( array(
	'id'           => $prefix . 'metabox_portfolio',
	'title'        => __( 'Contact Page For UK', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => 'uk_contact-page.php' ),
	) );
	
	
	
	//address 1
			$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Address 1 tagline ', 'cmb2' ),
	'id'         => $prefix . 'tagline_1',
	'type'       => 'text',
	) );
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Address 1', 'cmb2' ),
	'id'         => $prefix . 'address_1',
	'type'       => 'textarea_small',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Telephone 1', 'cmb2' ),
	'id'         => $prefix . 'tel_1',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Telephone link 1', 'cmb2' ),
	'id'         => $prefix . 'tel_link_1',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Email 1', 'cmb2' ),
	'id'         => $prefix . 'email_1',
	'type'       => 'text',
	) );
	
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Latitude 1', 'cmb2' ),
	'id'         => $prefix . 'lat_1',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Longitude 1', 'cmb2' ),
	'id'         => $prefix . 'long_1',
	'type'       => 'text',
	) );
	
	
		//address 2
		$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Address 2 tagline ', 'cmb2' ),
	'id'         => $prefix . 'tagline_2',
	'type'       => 'text',
	) );	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Address 2', 'cmb2' ),
	'id'         => $prefix . 'address_2',
	'type'       => 'textarea_small',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Telephone 2', 'cmb2' ),
	'id'         => $prefix . 'tel_2',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Telephone link 2', 'cmb2' ),
	'id'         => $prefix . 'tel_link_2',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Email 2', 'cmb2' ),
	'id'         => $prefix . 'email_2',
	'type'       => 'text',
	) );
	
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Latitude 2', 'cmb2' ),
	'id'         => $prefix . 'lat_2',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Longitude 2', 'cmb2' ),
	'id'         => $prefix . 'long_2',
	'type'       => 'text',
	) );
	
	
	
			//address 3
		$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Address 3 tagline ', 'cmb2' ),
	'id'         => $prefix . 'tagline_3',
	'type'       => 'text',
	) );
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Address 3', 'cmb2' ),
	'id'         => $prefix . 'address_3',
	'type'       => 'textarea_small',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Telephone 3', 'cmb2' ),
	'id'         => $prefix . 'tel_3',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Telephone link 3', 'cmb2' ),
	'id'         => $prefix . 'tel_link_3',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Email 3', 'cmb2' ),
	'id'         => $prefix . 'email_3',
	'type'       => 'text',
	) );
	
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Latitude 3', 'cmb2' ),
	'id'         => $prefix . 'lat_3',
	'type'       => 'text',
	) );
	
	
	$cmb_contact_page_uk->add_field( array(
	'name'       => __( 'Longitude 3', 'cmb2' ),
	'id'         => $prefix . 'long_3',
	'type'       => 'text',
	) );
	
	
	

	
	
}
//apslo_contact_uk_page_metabox

//============== end section contact UK ===============


//=============  section seo ==========================

add_action( 'cmb2_admin_init', 'apslo_register_seo_metabox' );



function apslo_register_seo_metabox() {
	
	$prefix = 'apslo_seo_';
	
	
	
	$cmb_seo_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_seo_box',
	'title'         => __( 'SEO settings', 'cmb2' ),
	'object_types'  => array( 'page', 'post', 'cases', 'services'), // 	Post type
	'on_front'        => true,
	'closed'     => true,
	// 	'show_on_cb' => 'yourprefix_show_if_front_page',
	//'	show_on' => array( 'key' => 'page-template', 'value' => 'portfolio.php' ),
		// 'priority'   => 'high',
		// 'show_names' => true,
	) );

	$cmb_seo_page->add_field( array(
    'name'       => __( 'Page title', 'cmb2' ),
    'desc'       => __( 'leave blank for default', 'cmb2' ),
    'id'         => $prefix . 'title',
    'type'       => 'text',
  ) );

  $cmb_seo_page->add_field( array(
    'name'       => __( 'Page description', 'cmb2' ),
    'desc'       => __( 'description for meta description tag', 'cmb2' ),
    'id'         => $prefix . 'description',
    'type'       => 'textarea_small',
    // 'on_front'        => false, // Optionally designate a field to wp-admin only
  ) );


  $cmb_seo_page->add_field( array(
    'name'       => __( 'Page keywords', 'cmb2' ),
    'desc'       => __( 'keywords for meta keywords tag', 'cmb2' ),
    'id'         => $prefix . 'keywords',
    'type'       => 'textarea_small',
  ) );


  $cmb_seo_page->add_field( array(
    'name'       => __( 'Custom meta tags', 'cmb2' ),
    'desc'       => __( 'meta tags to be place in header', 'cmb2' ),
    'id'         => $prefix . 'custom_meta_tags',
    'type'       => 'textarea_code',
  //  'repeatable' => true,
  ) );

} // apslo_register_seo_metabox

//================= end section seo ====================



function pw_register_taxonomy_metabox() {
	$prefix = 'pw_sort_';

	$cmb_term = new_cmb2_box( array(
		'id'               => $prefix . 'edit',
		'title'            => __( 'Furniture type settings', 'cmb2' ), // Doesn't output for term boxes
	'object_types'     => array( 'term' ), // 	Tells CMB2 to use term_meta vs post_meta
	'taxonomies'       => array( 'sort', 'post_tag' ), // 	Tells CMB2 which taxonomies should have these fields
	'new_term_section' => true, // 	Will display in the "Add New Category" section
	) );
	
	
	
	
	$cmb_term->add_field( array(
	'name' => __( 'Image', 'cmb2' ),
	'id'   => $prefix . 'image',
	'type' => 'file',
	) );

	$cmb_term->add_field( array(
		'name' => __( 'Order', 'cmb2' ),
		'id'   => $prefix . 'order',
		'type' => 'text_small',
		) );
  
  	$cmb_term->add_field( array(
		'name' => __( 'Hide in UK', 'cmb2' ),
		'id'   => $prefix . 'hide_uk',
		'type' => 'checkbox',
		) );
	
	  $cmb_term->add_field( array(
		'name' => __( 'Hide in IE', 'cmb2' ),
		'id'   => $prefix . 'hide_ie',
		'type' => 'checkbox',
		) );
	
}


/*
* Wedding gifts list
*/
add_action( 'cmb2_admin_init', 'apslo_register_wedding_gifts_list' );


function apslo_register_wedding_gifts_list() {
	
	$prefix = 'apslo_gifts_';
	
	
	
	$cmb_gifts_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_box',
	'title'         => __( 'Wish list', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => 'wedding_gift_list_page.php' ),
	) );
	
	
 
	$group_field_gifts = $cmb_gifts_page->add_field( array(
		'id'          => $prefix . 'gifts',
		'type'        => 'group',
		'options'     => array(
		'group_title'   => __( 'Products {#}', 'cmb2' ),
		'add_button'    => __( 'Add Another Product', 'cmb2' ),
		'remove_button' => __( 'Remove Product', 'cmb2' ),
		'sortable'      => true, // 	beta
		'closed'     => true, // 	true to have the groups closed by default
		),
		) );
		
  
 		$cmb_gifts_page->add_group_field( $group_field_gifts, array(
		'name' => __( 'Product name', 'cmb2' ),
		'id'   => 'name',
		'type' => 'text',
		) ); 
  
		
		$cmb_gifts_page->add_group_field( $group_field_gifts, array(
		'name' => __( 'Product Image', 'cmb2' ),
		'id'   => 'image',
		'type' => 'file',
		) );
    
  
   	$cmb_gifts_page->add_group_field( $group_field_gifts, array(
		'name' => __( 'Product link', 'cmb2' ),
		'id'   => 'link',
		'type' => 'text',
		) );

	
	
	
}
// apslo_register_product_page

/*
* end wedding gifts list
*/


/*
* Wedding registry
*/
add_action( 'cmb2_admin_init', 'apslo_register_wedding_registry' );


function apslo_register_wedding_registry() {
	
	$prefix = 'apslo_registry_';
	
	
	
	$cmb_wed_register = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_box',
	'title'         => __( 'Images', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => 'wedding_registry_page.php' ),
	) );
	
	
 

  $cmb_wed_register->add_field( array(
    'name'       => __( 'Image 1', 'cmb2' ),
    'id'         => $prefix . 'image_1',
    'type'       => 'file',
  ) );
  
    $cmb_wed_register->add_field( array(
    'name'       => __( 'Image 2', 'cmb2' ),
    'id'         => $prefix . 'image_2',
    'type'       => 'file',
  ) );
  
    $cmb_wed_register->add_field( array(
    'name'       => __( 'Image 3', 'cmb2' ),
    'id'         => $prefix . 'image_3',
    'type'       => 'file',
  ) );
  
    $cmb_wed_register->add_field( array(
    'name'       => __( 'Image 4', 'cmb2' ),
    'id'         => $prefix . 'image_4',
    'type'       => 'file',
  ) );
	
	
	
}
// apslo_register_wedding_registry

/*
* end weding registry
*/


/*
* Ventura TV
*/
// add_action( 'cmb2_admin_init', 'pw_fc_ventura_tv' );


function pw_fc_ventura_tv() {
	
	$prefix = 'pw_tv_';
	
	$cmb_portfolio_video_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_tv',
	'title'         => __( 'Ventura TV', 'cmb2' ),
	'object_types'  => array( 'video_tv'), // 	Post type
	'on_front'        => true,
	) );


	
	
	$cmb_portfolio_video_page->add_field( array(
		'name' => __( 'Video No', 'cmb2' ),
		'description' => __( 'Video number from YouTube', 'cmb2' ),
		'id'   => $prefix . 'video',
		'type' => 'text',
	) );
  
    $cmb_portfolio_video_page->add_field( array(
		'name' => __( 'Hide in UK', 'cmb2' ),
		'id'   => $prefix . 'hide_uk',
		'type' => 'checkbox',
		) );
	
}


/*
* end ventura TV
*/

/*
* Show Ventura TV in UK
*/

add_action( 'cmb2_admin_init', 'pw_fc_ventura_tv_hidde_uk' );

function pw_fc_ventura_tv_hidde_uk() {
	
	$prefix = 'pw_tv_uk_';
	
	$cmb_portfolio_video_page_uk = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_tv_uk',
	'title'         => __( 'Ventura TV hidde in UK settings', 'cmb2' ),
	'object_types'  => array( 'video_tv'), // 	Post type
	'on_front'        => true,
	) );


	
    $cmb_portfolio_video_page_uk->add_field( array(
		'name' => __( 'Hide in UK', 'cmb2' ),
		'id'   => $prefix . 'hidde',
		'type' => 'checkbox',
		) );
	
}


/*
* end Show Ventura TV in UK
*/





add_action( 'cmb2_admin_init', 'pw_register_taxonomy_metabox' );






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

add_action( 'cmb2_admin_init', 'international_front_page_uk' );


function international_front_page_uk() {
	
	$prefix = 'uk_';
		
	
	$cmb_uk = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_front_page',
		'title'         => __( 'Front Page Settings', 'cmb2' ),
		'object_types'  => array( 'page'), // 	Post type
    'context'      => 'normal',
	  'priority'     => 'high',
		'show_on' => array( 'key' => 'page-template', 'value' => 'uk_front-page.php' ),
		) );


		$cmb_uk->add_field( array(
			'name'       => __( 'Tagline 1', 'cmb2' ),
			'id'         => $prefix . 'tagline_1',
			'description' => __( 'Text under video on home page', 'cmb2' ),
			'type' => 'textarea_small',
			) );


			$pages = get_pages();
			
			$apslo_pages_array = array();
			
			foreach ( $pages as $page ) {
				
				$apslo_pages_array[ get_page_link( $page->ID ) ] =  __( $page->post_title , 'cmb2' );
				
			}
			
			$cmb_uk->add_field( array(
			'name'             => 'Portfolio Page',
			'desc'             => 'Select an portfolio page',
			'id'               => $prefix . 'portfolio_link',
			'type'             => 'select',
			'show_option_none' => true,
			'default'          => 'custom',
			'options'          => $apslo_pages_array,
		
			
			) );			



		$cmb_uk->add_field( array(
			'name'       => __( 'Profile text', 'cmb2' ),
			'id'         => $prefix . 'profile_text',
			'type' => 'textarea',
			) );		
			
			
			$cmb_uk->add_field( array(
				'name'       => __( 'Profile image', 'cmb2' ),
				'id'         => $prefix . 'profile_image',
				'type' => 'file',
				) );	


				$cmb_uk->add_field( array(
					'name'       => __( 'Tagline 2', 'cmb2' ),
					'id'         => $prefix . 'tagline_2',
					'type' => 'textarea_small',
				) );		


	// 	$group_field_id is the field id string, so in this case: $prefix . 'service_'
	$group_field_1 = $cmb_uk->add_field( array(
		'id'          => $prefix . 'videos_',
		'type'        => 'group',
		'description' => __( 'Generates videos displayed on front page', 'cmb2' ),
		'options'     => array(
		'group_title'   => __( 'Video boxes {#}', 'cmb2' ),
		'add_button'    => __( 'Add Another box', 'cmb2' ),
		'remove_button' => __( 'Remove box', 'cmb2' ),
		'sortable'      => true, // 	beta
		 	'closed'     => true, // 	true to have the groups closed by default
		),
		) );
		
		
		$cmb_uk->add_group_field( $group_field_1, array(
		'name' => __( 'Poster', 'cmb2' ),
		'id'   => 'image',
		'type' => 'file',
		) );

		$cmb_uk->add_group_field( $group_field_1, array(
		'name' => __( 'Video No', 'cmb2' ),
		'description' => __( 'Video number from Vimeo', 'cmb2' ),
		'id'   => 'video',
		'type' => 'text',
		) );

		$cmb_uk->add_group_field( $group_field_1, array(
		'name' => __( 'Video description', 'cmb2' ),
		'id'   => 'box_desc',
		'type' => 'text',
		) );


//added
		$cmb_uk->add_group_field( $group_field_1, array(
		'name' => __( 'Gallery image 1', 'cmb2' ),
		'id'   => 'image_gal_1',
		'type' => 'file',
		) );

		$cmb_uk->add_group_field( $group_field_1, array(
		'name' => __( 'Gallery image 2', 'cmb2' ),
		'id'   => 'image_gal_2',
		'type' => 'file',
		) );
	
			$cmb_uk->add_group_field( $group_field_1, array(
		'name' => __( 'Gallery image 3', 'cmb2' ),
		'id'   => 'image_gal_3',
		'type' => 'file',
		) );


		$cmb_uk->add_group_field( $group_field_1, array(
		'name' => __( 'Box number', 'cmb2' ),
		'id'   => 'no',
		'type' => 'text_small',
		) );
	
	
	
	
	
}//international_front_page_uk

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//================== section PODCASTS================
add_action( 'cmb2_admin_init', 'pw_fc_single_podcasts' );


function pw_fc_single_podcasts() {
	
	$prefix = 'pw_podcast';
	
	$cmb_podcast_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_podcast_box',
	'title'         => __( 'Podcast settings', 'cmb2' ),
	'object_types'  => array( 'podcasts'), // 	Post type
	'on_front'        => true,
	) );

	
	$cmb_podcast_page->add_field( array(
		'name' => __( 'Podcast embed code', 'cmb2' ),
		'id'   => $prefix . 'code',
		'type' => 'textarea_code',
	) );


	
	
}
// pw_fc_single_podcasts


//===========================
add_action( 'cmb2_admin_init', 'apslo_register_html_page' );


function apslo_register_html_page() {
	
	$prefix = 'html_page_';
	
	
	$cmb_workshop_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_html',
	'title'         => __( 'HTML code', 'cmb2' ),
	'object_types' => array( 'page'),
	'context'      => 'normal',
	'priority'     => 'high',
	'show_names'   => true,
	'show_on' => array( 'key' => 'page-template', 'value' => 'page-html.php' ),
	) );
	
	

		$cmb_workshop_page->add_field( array(
		'name' => __( 'HTML code', 'cmb2' ),
		'id'   => $prefix . 'code',
		'type' => 'textarea_code',
	) );
	
	
}


//============









?>
