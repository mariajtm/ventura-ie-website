<?php

/**
* Template Name: Portfolio Archive Page
* Selectable from a dropdown menu on the edit page screen. All images will be in lightbox
*/
?>


<?php get_header(); ?>


<style>
  @media screen and (max-width: 780px) {
    .portfolio_nav{
      min-height:80px;
    }
    .mobile_clr{
      height:20px;
    }
  }
  
</style>



<div class="clear" style="height:0px"></div>

<div class="container portfolio_container">

<div class="col-sm-12 tagline" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="500">
    <h1>Portfolio Archive</h1>
</div>
<div class="clear" style="height:0px"></div>



 <div class="clear" style="height:0px"></div>
 


<div class="col-sm-12 portfolio_nav" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="300">




<div class="clear mobile_clr" style="height:0px"></div>
</div><!-- portfolio_nav -->

<div class="clear" style="height:0px"></div>

<div class="col-sm-12 portfolio_holder">


<?php
$mainUrl = get_template_directory_uri() . '/';


$type = 'portfolio';
$args=array(
  'post_type' => $type,
  'post_status' => 'hidden',
  'posts_per_page' => -1,
);

$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post();
  $format = get_post_format( $post->ID );
  
  if($format == 'video'){
    $desc = get_post_meta( $post->ID, 'pw_portfolio_video_box_desc', true );
    $video = get_post_meta( $post->ID, 'pw_portfolio_video_video', true );
    $poster= get_post_meta( $post->ID, 'pw_portfolio_video_image', true );
 //   include('partials/loop_video.php');
  }else{
    $desc = get_post_meta( $post->ID, 'pw_portfolio_gallery_box_desc', true );
    $poster= get_post_meta( $post->ID, 'pw_portfolio_gallery_image', true );
    $content = get_the_content();

   // include('partials/loop_gallery_new.php');
  }
  
 ?> 
  <!-- new -->
  <!--
  <a href="<?php the_permalink();?>">
<div class="col-sm-6 front_portfolio_out">
    <div class="col-sm-12 front_portfolio_in">
        <div class="portfolio_cover" style="background-image:url('<?php echo get_the_post_thumbnail_url();?>')">
            <img src="<?php echo get_the_post_thumbnail_url();?>"/>
            <div class="cover"></div>
        </div>

    <div class="clear" style="height:0px"></div>
    <h3><?php echo get_the_title();?></h3>
    </div>
</div>
</a>
-->
  
  <div class="col-sm-6 portfolio_item_out">
    <div class="col-sm-12 portfolio_item_in">
<a href="<?php the_permalink();?>">   
        <div class="col-sm-12 portfolio_item_poster" style="background-image:url('<?php echo $poster;?>');">
        <img class="portfolio__poster" src="<?php echo get_template_directory_uri();?>/img/default_portfolio.jpg"/>
        <div class="cover"></div>
        <div class="overlay">
         <!-- <img src="<?php echo get_template_directory_uri();?>/img/gallery.png"/> -->
          </div>
        </div>
      </a>
        <div class="col-sm-12 portfolio_item_desc">
            <p> 
                <b><?php echo get_the_title();?></b>
              <span style="padding-left:10px;">
                <a href="<?php the_permalink();?>" style="color:#000;">read more...</a>
              </span>
            </p>
        </div>
    </div>
</div>
  
  
  
<!-- end new -->
  
<?php 
 endwhile;
}
?>








</div><!-- portfolio_holder -->





</div><!-- portfolio_container -->


<div class="clear" style="height:120px"></div>









<?php get_footer(); ?>










</body>
</html>


