<?php

ob_start();

require_once ( get_stylesheet_directory() . '/fc/emails.php');
require_once ( get_stylesheet_directory() . '/fc/extra_fileds.php' );
require_once ( get_stylesheet_directory() . '/fc/paw_option_page.php' );
require_once ( get_stylesheet_directory() . '/fc/post_types.php' );
require_once ( get_stylesheet_directory() . '/fc/wp_bootstrap_navwalker.php');
require_once ( get_stylesheet_directory() . '/fc/rest_helper.php');

require_once ( get_stylesheet_directory() . '/fc/hospitality.php');
require_once ( get_stylesheet_directory() . '/fc/masterclass_fc.php');
require_once ( get_stylesheet_directory() . '/fc/ventura_tv.php');
require_once ( get_stylesheet_directory() . '/fc/ventura_podcasts.php');

add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array( 'video' ) );

function wpcodex_add_excerpt_support_for_pages() {
	add_post_type_support( 'portfolio', 'video' );
}
add_action( 'init', 'wpcodex_add_excerpt_support_for_pages' );

add_filter('show_admin_bar', '__return_false');


register_sidebar( array(
    'name'=>'Footer',
    'id'            => 'pw_sidebar_footer',
  	'description'   => 'Widget area for the footer.',
  	'class'         => '',
  	'before_widget' => '<div id="%1$s" class="col-sm-4 footer_col_iner_l footer-col_iner_l_1 footer-col_iner_l_%2$s">',
  	'after_widget'  => '</div>',
  	'before_title'  => '<p><b>',
  	'after_title'   => '</b><br />'
) );


register_sidebar(array( // Blog widget area
'name'=>'Post sidebar',
'id'            => 'pw_sidebar_posts',
'description' => 'Widget area for the posts.',
'before_widget' => '<div id="%1$s" class="widget %2$s">',
'after_widget' => '</div>',
'before_title' => '<div class="title widgettitle">',
'after_title' => '</div>',
));


register_sidebar( array(
    'name'=>'Footer UK',
    'id'            => 'pw_sidebar_footer_uk',
  	'description'   => 'Widget area for the footer in UK.',
  	'class'         => '',
  	'before_widget' => '<div id="%1$s" class="col-sm-4 footer_col_iner_l footer-col_iner_l_1 footer-col_iner_l_%2$s">',
  	'after_widget'  => '</div>',
  	'before_title'  => '<p><b>',
  	'after_title'   => '</b><br />'
) );





register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'THEMENAME' ),
   // 'portfolio' => __( 'Portfolio Menu', 'THEMENAME' ),
   // 'products' => __( 'Products Menu', 'THEMENAME' ),
  'uk_menu' => __( 'UK Menu', 'THEMENAME' ),
  'world_menu' => __( 'World Menu', 'THEMENAME' ),
) );



function string_limit_words($string, $word_limit){
  $words = explode(' ', $string, ($word_limit + 1));
if(count($words) > $word_limit) {
  array_pop($words);
  echo implode(' ', $words)."...";
} else {
  echo implode(' ', $words); }
}


//set cookie
add_action('init', function() {
$blog_title = str_replace(' ','_', get_bloginfo('name'));
$cookeiName = $blog_title.'_cookie';
    if (!isset($_COOKIE[ $cookeiName ])) {
        setcookie( $cookeiName , 'yes', strtotime('+10 day'));
    }
});




// dla custom post type{} kriesi_pagination($loop->max_num_pages);
function kriesi_pagination($pages = '', $range = 2)   // kriesi_pagination();  http://www.kriesi.at/archives/how-to-build-a-wordpress-post-pagination-without-plugin
{
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}



function add_menuclass($ulclass) {
return preg_replace('/<a /', '<a class="animsition-link"', $ulclass, 1);
}
// add_filter('wp_nav_menu','add_menuclass');




function my_login_logo() { ?>
    <style type="text/css">
    #login h1{
    background-color:#fafafa;
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.13);
    width:100%;
    padding-top:30px;
    }
    #login h1{
        background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png');
        padding-bottom: 0px;
        background-size: auto 80%;
        background-repeat: no-repeat;
       background-position: center;
       height:80px;
       margin-bottom:10px;
    }
    #login h1 a {
        display:none;
    }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );




// dashboard widget

function register_my_dashboard_widget() {
    global $wp_meta_boxes;

    wp_add_dashboard_widget(
        'my_dashboard_widget',
        'Support',
        'my_dashboard_widget_display'
    );

    $dashboard = $wp_meta_boxes['dashboard']['normal']['core'];

    $my_widget = array( 'my_dashboard_widget' => $dashboard['my_dashboard_widget'] );
    unset( $dashboard['my_dashboard_widget'] );

    $sorted_dashboard = array_merge( $my_widget, $dashboard );
    $wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
}
add_action( 'wp_dashboard_setup', 'register_my_dashboard_widget' );


function my_dashboard_widget_display() { ?>
<div style="width:50%;float:left">
<a href="#">
<img style="width:auto;height:60px;max-width:80%;" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" >
</a>
</div>
<div style="width:50%;float:right">
  <p>
    <b style="font-size:15px;">Ventura Marketing</b><br />
    email: fiona@venturamarketing.ie<br />
    web: <a href="http://venturamarketing.ie">venturamarketing.ie</a>
  </p>
</div>
<div style="clear:both;width:100%;"></div>

<hr />



 <?php }



 function remove_dashboard_meta() {
         remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
         remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
         remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
         remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
         remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
         remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
         remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
         remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    //     remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
 }
 add_action( 'admin_init', 'remove_dashboard_meta' );





 function new_excerpt_more( $more ) {
 //	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'your-text-domain') . '</a>';
 return ' ...';
 }
 add_filter( 'excerpt_more', 'new_excerpt_more' );



 function custom_excerpt_length( $length ) {
 	return 42;
 }
 add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


 function remove_editor() {

    if (isset($_GET['post'])) {
        $id = $_GET['post'];
        $template = get_post_meta($id, '_wp_page_template', true);

        if($template == 'front-page.php'){
          remove_post_type_support( 'page', 'editor' );
        }
        if($template == 'portfolio.php'){
          remove_post_type_support( 'page', 'editor' );
        }
        if($template == 'contact.php'){
          remove_post_type_support( 'page', 'editor' );
        }
    }

    $d = is_front_page();

}
add_action('init', 'remove_editor');


/*
add_action( 'edit_form_after_title', 'wpse_94626_front_page_detection' );

function wpse_94626_front_page_detection(){
    global $post_ID, $post_type;
  //  if ( empty ( $post_ID ) or 'page' !== $post_type )
    //    return;
    if ( $post_ID === (int) get_option( 'page_on_front' ) ){
      print '<h2>This is the front page</h2>';
      remove_post_type_support( 'page', 'editor' );
    }
}
*/

/*
function ridizain_remove_widgets(){
	unregister_sidebar( 'sidebar-4' );
}
add_action( 'widgets_init', 'ridizain_remove_widgets', 11 );
*/


/*
* Popup cookie
*/

function ventura_cookies_popup() { 
// Time of user's visit
$visit_time = date('F j, Y g:i a');
 
// Check if cookie is already set
if(isset($_COOKIE['ventura_visit_time'])) {
 
// Do this if cookie is set 
function visitor_greeting() {
 
// Use information stored in the cookie 
$lastvisit = $_COOKIE['ventura_visit_time'];
$string .= 'false'; 

return $string;
}   
 
} else { 
 
// Do this if the cookie doesn't exist
function visitor_greeting() { 
//$string .= 'true';
// Set the cookie if is home page
  $currentURL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];  
 if($currentURL == 'http://ventura.ie/' || $currentURL == 'http://ventura.ie/uk/'){
  $string .= 'true'; 
}else{
  $string .= 'false';
}  
  
return $string;
}   
 
// Set the cookie if is home page
  $currentURL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  echo '<!-- '.$currentURL.' -->';

 if($currentURL == 'http://ventura.ie/' || $currentURL == 'http://ventura.ie/uk/'){
   echo '<!-- front page -->';
   setcookie('ventura_visit_time',  $visit_time, time()+86400);
 }else{
   echo '<!-- not front page -->';
 }
  

}
 
// Add a shortcode 
add_shortcode('greet_me', 'visitor_greeting');
 
} 
add_action('init', 'ventura_cookies_popup');

/**
 * ADD CUSTOM POST STATUS 'Hidden'
 */
function my_custom_status_creation(){
  register_post_status( 'hidden', array(
  'label'                     => _x( 'Hidden', 'post' ),
  'label_count'               => _n_noop( 'Hidden <span class="count">(%s)</span>', 'Hidden <span 
  class="count">(%s)</span>'),
  'public'                    => true,
  'exclude_from_search'       => false,
  'show_in_admin_all_list'    => true,
  'show_in_admin_status_list' => true
  ));
}
add_action( 'init', 'my_custom_status_creation' );


function add_to_post_status_dropdown(){
  global $post;
  if($post->post_type != 'portfolio')
  return false;
  $status = ($post->post_status == 'hidden') ? "jQuery( '#post-status-display' ).text( 'Hidden' ); jQuery( 
  'select[name=\"post_status\"]' ).val('hidden');" : '';
  echo "<script>
  jQuery(document).ready( function() {
  jQuery( 'select[name=\"post_status\"]' ).append( '<option value=\"hidden\">Hidden</option>' );
  ".$status."
  });
  </script>";
}
add_action( 'post_submitbox_misc_actions', 'add_to_post_status_dropdown');





?>
