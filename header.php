<!DOCTYPE html>
<?php
global $post;

    $direct_parent_id = $post->post_parent;
    $direct_parent = get_post($direct_parent_id); 
    $direct_parent_slug = $direct_parent->post_name;
?>

<?php
    if( $direct_parent_slug === 'uk' ){
     echo '<html ng-app="beeApp">';
    }elseif( $direct_parent_slug === 'world' ){
     echo '<html lang="en">';
    }else{
       echo '<html lang="en">';
    } 
?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  
    <?php
    // meta from theme options
    $options = get_option('wp78points_theme_options');
    $meta_keywords = get_post_meta( $post->ID, 'apslo_seo_keywords', true );
    $meta_description = get_post_meta( $post->ID, 'apslo_seo_description', true );
    $custom_meta_tags = get_post_meta( $post->ID, 'apslo_seo_custom_meta_tags', true );

    if ($meta_description) { ?>
    <meta name="description" content="<?php echo $meta_description; ?>">
    <?php }else{ ?>
    <meta name="description" content="<?php echo esc_attr( get_option('pageDescription'));?>">
    <?php } if ($meta_keywords) { ?>
    <meta name="keywords" content="<?php echo $meta_keywords;?>">
    <?php }else{ ?>
    <meta name="keywords" content="<?php echo esc_attr( get_option('pageKeywords'));?>">
    <?php } ?>

    <meta name="google-site-verification" content="XyMt3Ck0Kj34USPVzudodTBC4-97vT9RTwVRJwKaewY" />
    <meta name="p:domain_verify" content="9e078c8ea241a71b162266ffa2967486"/>

    <link rel="shortcut icon" href="<?php echo esc_attr( get_option('paw_favico'));?>"/>
    <link rel="icon" href="<?php echo esc_attr( get_option('paw_favico'));?>" type="image/x-icon">
  
  
  <?php
  //IF uk REDIRECT TO MAIN WEBSITE
    if( $direct_parent_slug === 'uk' ){
      if($post->post_name == 'uk'){
        echo '<script>window.location.replace("https://ventura.ie/");</script>';
      }else{
        echo '<script>window.location.replace("https://ventura.ie/'.$post->post_name.'");</script>';
      }
     
    } 
?>


    <?php
    $meta_page_title = get_post_meta( $post->ID, 'apslo_seo_title', true );
    if ($meta_page_title) { ?>
    <title><?php echo bloginfo('name');?> | <?php echo $meta_page_title; ?></title>
    <?php }else{ ?>
    <title><?php echo bloginfo('name');?> | <?php the_title(); ?></title>
    <?php }?>

<link href="https://fonts.googleapis.com/css?family=Muli:200,300,400,700|Playfair+Display:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

<link href="https://venturastatic.s3-eu-west-1.amazonaws.com/assets/bootstrap/css/bootstrap.min.css?ver=1.0.0" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?ver=1.0.0" rel="stylesheet">

<link href="https://venturastatic.s3-eu-west-1.amazonaws.com/css/animate.css?ver=1.0.0" rel="stylesheet">

<!--fancybox -->
<!--
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
    -->
<link rel="stylesheet" type="text/css" href="https://venturastatic.s3-eu-west-1.amazonaws.com/assets/fancy3/jquery.fancybox.min.css?ver=1.0.0">
<!--fancybox -->


<link href="https://venturastatic.s3-eu-west-1.amazonaws.com/css/style.css?ver=1.0.0" rel="stylesheet">
  
  
  
  
 <style>
   
   .product_pinterest{
     position:absolute;
   }
   .product_pinterest > .btn-primary{
     min-width:60px;
   }
   
   .footer_social2{
     text-align:right;
     padding-right:0px;
     padding-left:0px;
     padding-top:40px;
     padding-bottom:20px;
   }
   
   .fo{
     margin:10px;
   }
   
   .fo a{
     font-size:30px;
     color:#fff;
   }
   
   .fo a:hover{
     color:#CCD1D5;
     text-decoration:none;
   }
   
   @media screen and (max-width: 780px) {
        .footer_social2{
     text-align:center;
   }
   }
   .single_post_container{
    padding-top: 150px;
   }
   

@media screen and (max-width: 780px) {
  
.navbar_big {
    min-height: 50px;
}
  
  
  .navbar_big .navbar-brand {
    padding-top: 5px;
    padding-left: 0px;
}
  
  .navbar_big .navbar-brand img {
    height: 40px;
    width: auto;
}
  
h1 {
font-size: 20px;
}
  
  .section_3_contaienr .tagline h1 {
    font-size: 20px;
}
  
.pw_box_paragraph p {
    font-size:16px;
}  
  
.pw_button_ar_left {
    font-size: 12px;
    letter-spacing: 0.14em;
}
  
.pw_button_ar_left:after {
    display: inline-block;
    position: relative;
    top: 1px;
    content: "";
    width: 20px;
    height: 10px;
  background-size:contain;
}  

.pw_button_ar_left:hover:after {
  width: 20px;
    height: 10px; 
  } 
  
.portfolio_nav ul li {
    display: inline;
}  
  
  
.portfolio_nav ul li a {
    padding: 5px;
    margin: 1px;
    border: 1px solid #958D8A;
  width:49%;
  float:left;
}  
  
.portfolio_nav ul li:after {
    content: "";
    width: 0px;
    height: 0px;
      display: inline;
}
  
  
.portfolio_container {
  padding:0px;
  padding-top: 100px;
}  
.single_post_container {
  padding-top: 100px;
}  
  
  .portfolio_nav {
    padding:10px;
    padding-bottom: 0px;
    padding-top:0px;
}
  
.portfolio_holder {
  padding:0px;
    padding-top: 40px;
}  
  
  
 .portfolio_item_desc {
    padding-bottom: 10px;
} 
  
  .section_3_contaienr{
    padding-left:0px;
    padding-right:0px;
  }  
  .front_videos_holder{
    padding:0px;
  }
  
  .section_4_container{
    padding:0px;
    padding-bottom:20px;
  } 
  
}
   
 podcast_button {
    position: absolute;
    top: 10px;
    right: 10px;
    z-index: 9999;
    border-radius: 0px;
    color: #958D8A;
}  
   
   
@media screen and (max-width: 990px){

.loop_post_col_img, .loop_post_col_text {
  width:100%;
} 
  
  .main_cover{
    display:none;
  }
   
}
   
    .post-password-form{
    text-align:center;

  } 
   
   @media screen and (max-width: 1200px){
   .tv_button span{
     display:none;
   }
     
     .podcast_button span{
     display:none;
   }
    
     
   }
   

 .featured_box_col_right h2 {
    padding-top: 0px;
    margin-top: 0px;
   font-size:28px;
}  
   
  @media screen and (max-width: 400px){ 
   .single_post_title{
     font-size:17px;
   }
   }  
   
   
 </style>


<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "http://ventura.ie/",
  "name": "Ventura Design",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+353 1 820 8480",
    "contactType": "Head Office/Showroom"
  }
}
</script>
  
  
  <!-- linked in -->
  <!--
<script type="text/javascript"> _linkedin_data_partner_id = "205570"; </script>
  <script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> 
  <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=205570&fmt=gif" /> </noscript>  
-->
   <!-- // linked in -->

<?php if ( !is_front_page() ) { ?>
<style>

</style>
<?php } ?>

<!-- custom meta tags -->
<?php echo $custom_meta_tags; ?>
<!-- //custom meta tags -->
 <!-- Google Analytics -->
<?php // echo get_option('pageGoogle');?>
<!-- // Google Analytics -->
  
  
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 // ga('create', 'UA-22699002-1', 'auto');
  
  ga('create', 'UA-22699002-1', 'auto', {'allowLinker': true});
  ga('require', 'linker');
  ga('linker:autoLink', ['shop.ventura.ie']);
  
  ga('send', 'pageview');
</script>
<!-- // Google Analytics -->
  

<script>
//   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//   })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

//   ga('create', 'UA-22699002-1', 'auto');
//   ga('send', 'pageview');

</script>
  
  
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '197364948530080'); 
fbq('track', 'PageView');
</script>

<noscript>

<img height="1" width="1" 

src="https://www.facebook.com/tr?id=197364948530080&ev=PageView

&noscript=1"/>

</noscript>
<!-- End Facebook Pixel Code -->

  
<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>
  
  
  <style>
  .tv_button{
    position:absolute;
    top:10px;
    right:10px;
    z-index:9999;
    border-radius:0px;
    color:#958D8A;
  }
  
  .tv_button img{
    height:20px;
    margin-top:-9px;
    margin-right:5px;
    width:auto;
  }
  
  .tv_button div{
    padding-top:10px;
  }
  
  .navbar-default .navbar-toggle {
    border-radius: 0px;
    border-color: #ccc;
}
  
  @media only screen and (max-width: 1200px) {
    .tv_button{
      position:fixed;
    top:8px;
    right:70px;
  }
  }
    
    
  .podcast_button{
    position:absolute;
    top:10px;
    right:160px;
    z-index:9999;
    border-radius:0px;
    color:#958D8A;
  }
  
  .podcast_button img{
    height:20px;
    margin-top:-9px;
    margin-right:5px;
    width:auto;
  }
  
  .podcast_button div{
    padding-top:10px;
  }
  

  @media only screen and (max-width: 1200px) {
    .podcast_button{
      position:fixed;
    top:8px;
    right:120px;
  }
  }    
    
    
     
  .shop_button{
    position:absolute;
    top:10px;
    right:363px;
    z-index:9999;
    border-radius:0px;
    color:#958D8A;
  }
  
  .shop_button img{
    height:20px;
    margin-top:-9px;
    margin-right:5px;
    width:auto;
  }
  
  .shop_button div{
    padding-top:10px;
  }
  

  @media only screen and (max-width: 1200px) {
    .shop_button{
      position:fixed;
    top:8px;
    right:170px;
  }
  }    
    
    @media screen and (max-width: 1200px){
        .shop_button span {
            display: none;
        }    
    } 
    
    
    
  @media only screen and (max-width: 740px) {
    .tv_button{
      position:fixed;
    top:8px;
    right:60px;
  }
  .podcast_button{
    position:fixed;
    top:8px;
    right:108px;
    }
   
  .shop_button{
    position:fixed;
    top:8px;
    right:156px;
    }
    
    .shop_button img, .podcast_button img, .tv_button img{
      margin-right:0px;
    }    
  }    

    
  </style>
  
  <style>
    

  
@media screen and (max-width: 1280px) {
  

.navbar_big .nav {
    margin-top: 50px;
}
  
  .navbar_big .navbar-brand img {
    height: 40px;
    width: auto;
}
  


}
    
    
@media screen and (max-width: 1190px) {
  
  
  .navbar-default .navbar-nav > li > a {
    color: #958D8A;
    font-size: 12px;
    font-weight: 300;
    padding-top:10px;
    padding-bottom:10px;
}

}    
    
    
@media screen and (max-width: 1138px) {
  
  
.navbar_big .nav {
    margin-top: 50px;
}
  
    .navbar-default .navbar-nav > li > a {
    font-size: 10px;
    font-weight: 300;
    padding-top:0px;
    padding-bottom:0px;
}

}  
    
    
   @media screen and (max-width: 1027px) {
  
  
.navbar_big .nav {
    margin-top: 0px;
}
  

} 
    
    
  </style>
  
  

<?php  wp_head() ?>
  
  
  
  
 <?php if ( is_front_page() ) { ?>
  
<?php } ?>
  
  <!-- mailchimp
  <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/0e083ebf89336ac206fe34718/706d7dd101ac5a724a69b6f40.js");</script>
     // mailchimp -->
  
  <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/0e083ebf89336ac206fe34718/06294a5251ebe2a988b7b3162.js");</script>

</head>

<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<?php
$mainUrl = get_template_directory_uri() . '/';
?>

<?php
global $post;
?>

<body data-spy="scroll" data-target=".navbar" data-offset="50">
<!-- <div id="main_cover" class="cover"></div> -->


 <nav class="navbar navbar-default navbar-fixed-top navbar_big">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php 
     // if ( is_front_page() ) { $logo_link = '#home'; }else{ $logo_link = home_url();}
      
    //international menu distribution
//     $direct_parent_id = $post->post_parent;
//     $direct_parent = get_post($direct_parent_id); 
//     $direct_parent_slug = $direct_parent->post_name;
      
    if( $direct_parent_slug === 'uk' ){
      $logo_link = home_url('uk');
      $logo_image = '/img/logo_uk.png?ver=1.0.0';
    }elseif( $direct_parent_slug === 'world' ){
      $logo_link = home_url();
      $logo_image = '/img/logo.png?ver=1.0.0';
    }else{
      $logo_link = home_url();
      $logo_image = '/img/logo.png?ver=1.0.0';
    }   
      
      
      ?>
      <a class="navbar-brand" href="<?php echo $logo_link;?>">
      <img src="<?php echo get_template_directory_uri(); ?><?php echo $logo_image;?>"/>
      </a>
    </div>

    <!-- menu go hire -->
  <?php
  //  include_once('main_menu.php');

    
   if( $direct_parent_slug === 'uk'){
      include_once('uk_menu.php');
    }elseif( $direct_parent_slug === 'world' ){
      include_once('world_menu.php');
    }else{
      include_once('main_menu.php');
    }
  
    
  ?>
    


  </div><!-- /.container-fluid -->
  <div class="container-fluid " style="background-color: #E8E3DE;">
  <div class="col-lg-12 text-center" style="padding: 0.25em;" >
  <a href="<?php echo get_site_url();?>/sale">
    <p style="font-size:12px;margin:0px; color: #000;">Excellent News!
    Our Luxury Furniture For 20% Less This July. <br>
T&Cs apply.</p>
</a>
  </div>
</div>
    
</nav>
<!--  
  
<a href="https://shop.ventura.ie/" class="shop_button btn btn-default">
  <img src="<?php echo get_template_directory_uri();?>/img/shop_icon.png" /> <span>VENTURA<span style="font-weight:800;"> SHOP</span></span>
</a>  
  
<a href="https://ventura.ie/ventura-podcast/" class="podcast_button btn btn-default">
  <img src="<?php echo get_template_directory_uri();?>/img/podcast_icon.png" /> <span>VENTURA<span style="font-weight:800;"> PODCAST</span></span>
</a>
  

 <?php if( $direct_parent_slug === 'uk'){ ?>
  
<a href="https://ventura.ie/uk/tv-uk/" class="tv_button btn btn-default">
  <img src="<?php echo get_template_directory_uri();?>/img/retro-tv_icon.png" /> <span>VENTURA<span style="font-weight:800;">TV</span></span>
</a>
  
 <?php }elseif( $direct_parent_slug === 'world' ){ ?>
<a href="https://ventura.ie/tv/" class="tv_button btn btn-default">
  <img src="<?php echo get_template_directory_uri();?>/img/retro-tv_icon.png" /> <span>VENTURA<span style="font-weight:800;">TV</span></span>
</a>
 <?php }else{ ?>
<a href="https://ventura.ie/tv/" class="tv_button btn btn-default">
  <img src="<?php echo get_template_directory_uri();?>/img/retro-tv_icon.png" /> <span>VENTURA<span style="font-weight:800;">TV</span></span>
</a>
 <?php } ?> -->
  

