<div id="section_2" class="container-fluid section_2_container" style="background-image: url('<?php echo $profile_image;?>');">

<div class="col-sm-12 mobile_section_2_img"><img src="<?php echo $profile_image;?>"/></div>
<div class="col-sm-12 col-md-6 col-lg-5 section_2_col section_2_col_l" paw-on-mobile="false" paw-animate="slideInLeft" paw-delay="300">
<div class="section_2_col_l_iner">
<?php echo $profile_text;?> 
  <div class="clear" style="height:20px"></div>

  <div class="col-sm-12" style="padding:0px;" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="300">
  <a href="<?php echo $portfolio_link;?>" class="pw_button_ar_left">VIEW OUR GALLERY</a>
  </div>

  <div class="clear" style="height:20px"></div>
</div>
</div>
<div class="col-sm-12 col-md-6 col-lg-7 section_2_col section_2_col_r"></div>


</div><!-- section_2_container -->