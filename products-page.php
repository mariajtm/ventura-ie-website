<?php

/**
* Template Name: Products Page
* Selectable from a dropdown menu on the edit page screen. All images will be in lightbox
*/
?>


<?php get_header(); ?>





<div class="container products_box_1">
<div class="col-sm-12 tagline" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="500">
    <h1>Our Products</h1>
</div>
<div class="clear" style="height:0px"></div>
<div class="pw_box_paragraph" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="300">

<?php if (have_posts()) : ?>
 <?php while (have_posts()) : the_post(); ?>
 <?php $bottom_text =  get_post_meta( $post->ID, 'apslo_product_page_text', true );?>

<?php the_content();?>

<?php endwhile; ?>
<?php else : ?>

<?php endif; ?>

    
</div>  
</div><!-- products_box_1 -->

<div class="container-fluid products_holder">


<?php

// $taxonomies = get_taxonomies(); 
// foreach ( $taxonomies as $taxonomy ) {
//     echo '<p>' . $taxonomy . '</p>';
// }

?>


<?php
$terms = get_terms([
    'taxonomy' => 'sort',
    'hide_empty' => false,
    'order' => 'ASC',
    'meta_key' => 'pw_sort_order',
    'orderby' => 'pw_sort_order'
]);
//print_r($terms);
?>



<?php
foreach($terms as $term){ 
  
 // if($term->slug != 'candles' && $term->slug != 'paints' ){
   if($term->slug != 'bathrobes' ){

$image = get_term_meta( $term->term_id, 'pw_sort_image', true );   
$hide_in_ie = get_term_meta( $term->term_id, 'pw_sort_hide_ie', true ); 
if($hide_in_ie != 'on'){
?>

<a href="<?php echo get_site_url(); ?>/sort/<?php echo $term->slug;?>">
<div class="col-sm-4 col-md-3 product_list_item_out" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="100" style="margin-bottom:5px;">
<img src="<?php echo $image;?>"/>
<div class="product_list_item_product_info">
    <span>
    <h3><?php echo $term->name;?></h3>
    <p>CLICK TO VIEW</p>
    </span>
</div>
</div><!-- product_list_item_out -->
</a>


<?php } } }?>

  

<!-- <a href="https://ventura.ie/product/bathrobes/">
<div class="col-sm-4 col-md-3 product_list_item_out" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="100">
<img src="https://ventura.ie/wp-content/uploads/2019/11/Bathrobes.jpg"/>
<div class="product_list_item_product_info">
    <span>
    <h3>BATHROBES</h3>
    <p>CLICK TO VIEW</p>
    </span>
</div>
</div>
</a>  
 -->



</div><!-- products_holder -->



<div class="container products_box_2">
<div class="col-sm-12 tagline" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="300">

<h2><?php echo $bottom_text;?></h2>


</div>
<div class="clear" style="height:20px"></div>
<div class="pw_box_paragraph" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="500">
<a href="<?php echo get_site_url(); ?>/workshop" class="pw_button_ar_left">SEE OUR WORKSHOPS</a>
</div>  
</div><!-- products_box_2 -->
<div class="clear" style="height:60px"></div>








<?php get_footer(); ?>


</body>
</html>