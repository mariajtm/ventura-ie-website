<?php

/**
* Template Name: Contact Page
* Selectable from a dropdown menu on the edit page screen. All images will be in lightbox
*/
?>
<?php get_header(); ?>


<?php
  global $post;

$tagline_1 = get_post_meta( $post->ID, 'apslo_contact_tagline_1', true );
$address_1 = get_post_meta( $post->ID, 'apslo_contact_address_1', true );
$tel_1 = get_post_meta( $post->ID, 'apslo_contact_tel_1', true );
$tel_link_1 = get_post_meta( $post->ID, 'apslo_contact_tel_link_1', true );
$email_1 = get_post_meta( $post->ID, 'apslo_contact_email_1', true );
$lat_1 = get_post_meta( $post->ID, 'apslo_contact_lat_1', true );
$long_1 = get_post_meta( $post->ID, 'apslo_contact_long_1', true );


$tagline_2 = get_post_meta( $post->ID, 'apslo_contact_tagline_2', true );
$address_2 = get_post_meta( $post->ID, 'apslo_contact_address_2', true );
$tel_2 = get_post_meta( $post->ID, 'apslo_contact_tel_2', true );
$tel_link_2 = get_post_meta( $post->ID, 'apslo_contact_tel_link_2', true );
$email_2 = get_post_meta( $post->ID, 'apslo_contact_email_2', true );
$lat_2 = get_post_meta( $post->ID, 'apslo_contact_lat_2', true );
$long_2 = get_post_meta( $post->ID, 'apslo_contact_long_2', true );



$tagline_3 = get_post_meta( $post->ID, 'apslo_contact_tagline_3', true );
$address_3 = get_post_meta( $post->ID, 'apslo_contact_address_3', true );
$tel_3 = get_post_meta( $post->ID, 'apslo_contact_tel_3', true );
$tel_link_3 = get_post_meta( $post->ID, 'apslo_contact_tel_link_3', true );
$email_3 = get_post_meta( $post->ID, 'apslo_contact_email_3', true );
$lat_3 = get_post_meta( $post->ID, 'apslo_contact_lat_3', true );
$long_3 = get_post_meta( $post->ID, 'apslo_contact_long_3', true );

?>

<style>
  body{
    background-color:#F6F6F6;
    overflow-x:hidden;
  }
  .form_box_pw{
    width:500px;
    max-width:100%;
    margin-left:auto;
    margin-right:auto;
  }
</style>

<div class="container-fluid contact_box_1">

  <!--
<div class="col-sm-6 col-md-5 contact_box_1_col contact_box_1_col_l">
-->
  <div class="col-sm-12 col-md-12 contact_box_1_col contact_box_1_col_l" style="padding-top:50px;">
<div class="form_box_pw">
  
      
  <h2>
    Get In Touch
  </h2>
  <p> We welcome your visit to our showrooms, where you can experience our furniture collections and speak to a member of our team. 
    Click  <a href="<?php get_site_url()?>/all-showrooms" style="color:#000">here</a>  to see our showroom locations. </p>

  <p> Otherwise, please complete the information below and we will contact you shortly.   </p>
  
  <?php 
  $pw_get = $_GET;

$pw_product_query = '';
$pw_mess = '';
  
  if (array_key_exists('url', $pw_get)){
    $pw_mess = "I am interested in this product: ". sanitize_text_field($pw_get['url'] . " \n");
    $pw_product_query = 'selected="selected"';
  }
  
  $c2c = '';
  if($pw_get['concept2completion']){
    $c2c = 'selected="selected"';
  }  
  $homeDesign = '';
  if($pw_get['HomeDesign']){
    $homeDesign = 'selected="selected"';
  }   
  
  $styling = '';
   if($pw_get['d']){
    $d = 'selected="selected"';
  } 
  
  $architecture = '';
  if($pw_get['a']){
    $architecture = 'selected="selected"';
  } 
    
  // $online = '';
  // if($pw_get['o']){
  //   $online = 'selected="selected"';
  // } 
  
    
  // $int = '';
  // if($pw_get['int']){
  //   $int = 'selected="selected"';
  // } 
  
  ?>
  
<div id="alert">
   <div id="zoneSub" class="error"></div>
 </div>


    
            <form role="form" method="POST" id="pw_contact_form_3" class="pw_form" >
          <input type="hidden" class="form-control" name="subject" id="subject" value="Ventura web contact form">
          <input type="hidden" class="form-control" name="email_to" id="email_to" value="info">
          <input type="hidden" class="form-control" name="country" id="country" value="IE">
          <input type="hidden" class="form-control" name="tag" id="tag" value="ventura_ie_contact">
          
          
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
            </div>
          
          
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
            </div>
          
          
          
            <div class="form-group">
                <label>Phone</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Mobile" required>
            </div>

          <div class="form-group">
            <label>Type of enquiry</label>
            <select class="form-control" id="type" name="type" required>
              <option value="">Select...</option>
              <option <?php echo $pw_product_query;?> >Product enquiry</option>
              <!-- <option <?php echo $architecture;?>>Interior Architecture</option>
              <option <?php echo $d;?>>Interior Design</option> -->
              <option <?php echo $homeDesign;?> >Ventura Home Design Service</option> 
              <option <?php echo $c2c;?> >Concept to Completion Service</option>
              <option>Other</option>
            </select>
            </div>  
          
           <div class="form-group">
            <label>Source</label>
            <select class="form-control" id="source" name="source" required>
              <option value="">Select...</option>
              <option>Instagram</option>
              <option>RTE Designed for Life</option>
              <option>Newsletter</option>
              <option>Google</option>
              <option>Recommendation</option>
              <option>Facebook</option>
              <option>Pinterest</option>
              <option>Magazine</option>
              <option>Design Centre</option>
              <option>Event</option>
              <option>Other</option>
            </select>
            </div>

            <div class="form-group">
              <label>Newsletter</label>
              <select class="form-control" id="gdpr" name="gdpr" required>
                <option value="">Select...</option>
                <option value="Yes">Yes, send me new offers</option>
                <option value="No">No, I don't like to receive new offers</option>
              </select>
              </div>
          
          
            <div class="form-group">
                <label>Message</label>
              <textarea rows="5" cols="5" class="form-control" name="message" placeholder="Message"><?php echo $pw_mess;?></textarea>
            </div>

  
            <div class="clear" style="height:10px;"></div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">SUBMIT</button>
            </div>
          
          </form>  
    
     <div class="clear" style="height:20px;"></div> 
       <h2>
    Media Enquiries
  </h2>
  <p>
    For press or media enquiries for Ventura Design or to enquire about our podcast, Shut the Front Door, please contact our PR consultant <a href="mailto:grace@ventura.ie">grace@ventura.ie</a>
  </p> 


</div><!-- form_box_pw -->
  
  
</div><!-- contact_box_1_col_l -->
  



</div><!-- contact_box_1 -->



<div class="container-fluid contact_box_2">
<div class="col-sm-6 col-md-5 contact_box_2_col contact_box_2_col_l">
<div id="showrooms" class="col-sm-12 tagline">
    <h1>Visit our showrooms</h1>
</div>
<div class="clear" style="height:0px"></div>
    

<div class="col-sm-12 contact_addr">
<p>
<b><?php echo $tagline_1;?></b><br />
  
<?php echo nl2br($address_1);?><br />
<a href="tel:<?php echo $tel_link_1;?>">Tel: <?php echo $tel_1;?></a><br />
<a href="mailto:<?php echo $email_1;?>">Email: <?php echo $email_1;?></a>
</p>
<div class="clear" style="height:0px"></div>
<a href="#" id="direction_1" class="pw_button_ar_left">DIRECTIONS HERE</a>
</div><!-- contact_addr -->


<div class="col-sm-12 contact_addr">
<p>
<b><?php echo $tagline_2;?></b><br />
<?php echo nl2br($address_2);?><br />
<?php if($tel_2 != ''){ ?>
 <a href="tel:<?php echo $tel_link_2;?>">Tel: <?php echo $tel_2;?></a><br /> 
<?php } ?>
 <?php if($email_2 != ''){ ?>
<a href="mailto:<?php echo $email_2;?>">Email: <?php echo $email_2;?></a>
<?php } ?> 
</p>
<div class="clear" style="height:0px"></div>
<a href="#" id="direction_2"  class="pw_button_ar_left">DIRECTIONS HERE</a>
</div><!-- contact_addr -->


<div class="col-sm-12 contact_addr">
  <!--
<p>
<b><?php echo $tagline_3;?></b><br />
<?php echo nl2br($address_3);?><br />
<?php if($tel_3 != ''){ ?>
 <a href="tel:<?php echo $tel_link_3;?>">Tel: <?php echo $tel_3;?></a><br /> 
<?php } ?>
 <?php if($email_3 != ''){ ?>
<a href="mailto:<?php echo $email_3;?>">Email: <?php echo $email_3;?></a>
<?php } ?>    
</p>
<div class="clear" style="height:0px"></div>
<a href="#" id="direction_3"  class="pw_button_ar_left">DIRECTIONS HERE</a>
  -->
</div><!-- contact_addr -->



</div><!-- contact_box_2_col_l -->

<div id="map-canvas" class="col-sm-6 col-md-7 contact_box_2_col contact_box_2_col_r"></div><!-- contact_box_2_col_r -->

</div>



<div class="clear" style="height:0px"></div>




<div class="modal fade" tabindex="-1" role="dialog" id="pw_sending_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p style="color:#fff;">Sending message ...</p>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- px -->
<script type="text/javascript" src='https://i.ctnsnet.com/int/integration?pixel=53544739&nid=304040&cont=s' async="true"></script>
<!-- //px -->


<?php get_footer(); ?>

<script>

$(document).ready(function () {
console.log('loading PW Emails');

//====================
    
$("#pw_contact_form_3").submit(function () {

    console.log('submit');
            
$('#pw_sending_modal').modal({
        backdrop: 'static',
        keyboard: false
    });

     var form_data = $("#pw_contact_form_3").serialize();
     console.log('test pw_contact_form_3');
     console.log(form_data); 
     send_form_vnet(form_data);

    
    return false; // ne change pas de page
});


function send_form_vnet(form_data){
  var api_url = 'https://apiventura.com/vnet/api/public/form/post'
    $.ajax({
        method : "POST",
        url : api_url,
        data:form_data,
    }).then(function mySuccess(response) {
        console.log(response);
        window.location = "https://ventura.ie/thank-you/";
    }, function myError(response) {
        console.log('error');
        console.log(response);
        document.getElementById('zoneSub').innerHTML = '<div class="alert alert-danger" role="alert">Sorry. Message cannot be sent at this time.</div>';
    }); 
}

});

</script>


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCw4yw1_BksnheSJ7CMMBoZdqGkCnYoIzI"></script>

<script type="text/javascript">


$(document).ready(function(){
  
  var window_width = $(window).width();

  initialize_tab(pos1); //default map

$('#direction_1').click(function(e){
e.preventDefault();
console.log('direction_1');
initialize_tab(pos1); 
  
if(window_width <= 780){
  $('html, body').stop().animate({
            scrollTop: $('#map-canvas').offset().top -100
        }, 1500, 'swing'); 
}  
   
});

$('#direction_2').click(function(e){
e.preventDefault();
console.log('direction_2');
initialize_tab(pos2); 
  
if(window_width <= 780){
  $('html, body').stop().animate({
            scrollTop: $('#map-canvas').offset().top -100
        }, 1500, 'swing'); 
}   
  
});


$('#direction_3').click(function(e){
e.preventDefault();
console.log('direction_3');
initialize_tab(pos3); 
  
if(window_width <= 780){
  $('html, body').stop().animate({
            scrollTop: $('#map-canvas').offset().top -100
        }, 1500, 'swing'); 
}   
  
});



});   


//google maps /////////
var marker;
    var map;

    function initialize_tab(pw_position) {
        "use strict";
        var roadAtlasStyles = [

            {
                "stylers": [
                  { "saturation": -91 },
                  { "hue": "#958D8A" }
                ]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [
                    { "visibility": "off" }
                ]
            }

        ];


        var mapOptions = {
            zoom: 14,
            center: pw_position,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'usroadatlas']
            }
        };

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


        var image = 'img/map_pointer.png';
        marker = new google.maps.Marker({
            map: map,
       //     icon: image,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: pw_position
        });



        var styledMapOptions = {
            name: "Styled Map",
        };

        var usRoadMapType = new google.maps.StyledMapType(
            roadAtlasStyles, styledMapOptions);

        map.mapTypes.set('usroadatlas', usRoadMapType);
        map.setMapTypeId('usroadatlas');
    }

  <?php if( $lat_1 != ''){ ?>
    var pos1 = new google.maps.LatLng(<?php echo $lat_1;?>, <?php echo $long_1;?>);
      <?php }else{ ?>
  var pos1 = '';
   <?php } ?>
  
   <?php if( $lat_2 != ''){ ?>
    var pos2 = new google.maps.LatLng(<?php echo $lat_2;?>, <?php echo $long_2;?>);
    <?php }else{ ?>
  var pos2 = '';
   <?php } ?>
  
   <?php if( $lat_3 != ''){ ?>
    var pos3 = new google.maps.LatLng(<?php echo $lat_3;?>, <?php echo $long_3;?>);
  <?php }else{ ?>
  var pos3 = '';
   <?php } ?>
   



 
    
</script>


</body>
</html>
