<a href="#" data-toggle="modal" data-target="#galleryModal_<?php echo $post->ID;?>" data-pw-link-gallery="<?php echo $post->ID;?>">
<div class="col-sm-6 portfolio_item_out" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="0">
    <div class="col-sm-12 portfolio_item_in">
        <div class="col-sm-12 portfolio_item_poster" style="background-image:url('<?php echo $poster;?>');">
        <img class="portfolio__poster" src="<?php echo get_template_directory_uri();?>/img/default_portfolio.jpg"/>
        <div class="cover"></div>
        <div class="overlay"><img src="<?php echo get_template_directory_uri();?>/img/gallery.png"/></div>
        </div>
        <div class="col-sm-12 portfolio_item_desc">
            <p> 
                <b><?php echo get_the_title();?></b><br />
                <?php echo $desc;?>
            </p>
        </div>
    </div>
</div><!-- portfolio_item_out -->
<!-- <?php the_permalink() ?> -->
</a>


<?php
//
$content = get_the_content();

//preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $content, $last_image);

preg_match_all('/<img[^>]+>/i',$content, $all_images); 

$images_array = '[';
foreach( $all_images[0] as $key=>$value){
    if($key == 0){
        $first_image = $value;
    }else{
        $image_s = str_replace('"', '\'', $value );
        $images_array = $images_array . '"' . $image_s . '",';
    }

}

$images_array = $images_array . ']';
$images_array = str_replace(',]', ']', $images_array );



?>
<script id="images_array_<?php echo $post->ID;?>" type="text/cycle">
<?php echo $images_array;?>
</script>




<!-- Modal -->
<div class="modal modal-wide_gallery_2 fade" id="galleryModal_<?php echo $post->ID;?>" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog_gal modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <div class="clear" style="height:0px"></div>
       <div class="col-sm-12 gallery_frame_2">

    <div class="cycle-slideshow gallery_slideshow_2" 
    data-cycle-fx=scrollHorz
    data-cycle-timeout=0
    data-cycle-caption=".caption_<?php echo $post->ID;?>"
    data-cycle-caption-template="{{slideNum}} / <?php echo sizeof($all_images[0])  ;?>"
    data-cycle-pager="#custom-pager" 
    data-cycle-pager-template="<strong><a href=#> {{slideNum}} </a></strong>"
    data-cycle-loader=true
    data-cycle-progressive="#images_array_<?php echo $post->ID;?>"
    data-cycle-prev="#prev_<?php echo $post->ID;?>"
        data-cycle-next="#next_<?php echo $post->ID;?>"
        data-cycle-center-horz=true
    data-cycle-center-vert=true
       >
       <?php echo $first_image;?>

       </div>
       <div class="clear" style="height:0px"></div>
 

       <div class="col-sm-12 gallery_nav_holder_2">
        
       <span id="prev_<?php echo $post->ID;?>" class="gallery_nav_btn"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Prev </span>
        <span id="next_<?php echo $post->ID;?>" class="gallery_nav_btn"> Next <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></span>
       
          <div class="video_close_btn2" data-dismiss="modal">&times;</div>
       </div>


       </div>
       <div class="clear" style="height:0px"></div>
       
      </div>
    </div>
  </div>
</div>




<!--
<div class="modal modal-wide_gallery fade" id="galleryModal_<?php echo $post->ID;?>" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <div class="video_close_btn" data-dismiss="modal">&times;</div>
          <div class="clear" style="height:0px"></div>
       <div class="col-sm-12 gallery_frame">

    <div class="cycle-slideshow gallery_slideshow" 
    data-cycle-fx=scrollHorz
    data-cycle-timeout=0
    data-cycle-caption=".caption_<?php echo $post->ID;?>"
    data-cycle-caption-template="{{slideNum}} / <?php echo sizeof($all_images[0])  ;?>"
    data-cycle-pager="#custom-pager" 
    data-cycle-pager-template="<strong><a href=#> {{slideNum}} </a></strong>"
    data-cycle-loader=true
    data-cycle-progressive="#images_array_<?php echo $post->ID;?>"
    data-cycle-prev="#prev_<?php echo $post->ID;?>"
        data-cycle-next="#next_<?php echo $post->ID;?>"
       >
       <?php echo $first_image;?>

       </div>
       <div class="clear" style="height:0px"></div>
       <div class="center caption_<?php echo $post->ID;?>"></div>
       <div class="clear" style="height:10px"></div>
       <div class="col-sm-12 gallery_nav_holder">
       <span id="prev_<?php echo $post->ID;?>" class="gallery_nav_btn"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Prev </span>
        <span id="next_<?php echo $post->ID;?>" class="gallery_nav_btn"> Next <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></span>
       </div>
      

       </div>
       <div class="clear" style="height:0px"></div>
       
      </div>
    </div>
  </div>
</div>
-->