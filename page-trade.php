<?php

/**
* Template Name: Trade Page
* Selectable from a dropdown menu on the edit page screen. All images will be in lightbox
* This page template is use for paypal thank you and cancel page
*/
?>

<?php
// https://www.sweetpeaandwillow.com/trade-account/
?>



<?php get_header(); ?>

<style>
  
  body{
        background-color: #F6F6F6;
  }


    .hospitality_container{
    padding-top:40px;
 
  }
  
  .hospitality_form_box{
    min-height: 200px;
    width:450px;
    max-width:100%;
    margin-left:auto;
    margin-right:auto;
  }
  

  
   @media screen and (max-width: 780px) {
      
 .hospitality_container{
    padding-top:0px;
 
  }
  } 
  
  
</style>

<div class="clear" style="height: 100px;"></div>
<div class="col-sm-12 hospitality_text">
  <div class="container hospitality_container">
  

<?php if (have_posts()) : ?>
 <?php while (have_posts()) : the_post(); ?>

<div class="clear" style="height:0px;"></div>

<span style="text-align:center;">
 <?php the_content(); ?>   
</span>
<div class="clear" style="height:40px;"></div>
<div class="hospitality_form_box2">
  
<div id="alert">
   <div id="zoneSub" class="error"></div>
 </div>
  
<form role="form" action="sender.php" method="POST" id="pw_contact_trade" class="pw_form" >
<input type="hidden" class="form-control" name="subject" id="subject" value="Hospitality form">
<input type="hidden" class="form-control" name="form_email" id="form_email" value="fiona@venturamarketing.ie, info@ventura.ie">
  
  <div class="clear" style="height:0px;"></div>
  
  <div class="col-sm-12">
    <h2>
     Your Details
    </h2>
  </div>
  
  <div class="clear" style="height:10px;"></div>
  <div class="col-sm-6 form_col form-col_l">
    
      <div class="form-group">
        <label>Company Name <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="company" name="company" required>
      </div>
    
       <div class="form-group">
        <label>First Name <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="firstname" name="firstname" required>
      </div>
    
         <div class="form-group">
        <label>Email Address <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="email_1" name="email_1" required>
      </div>
  
    
       <div class="form-group">
        <label>Telephone <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="phone_1" name="phone_1" required>
      </div>   
    
       <div class="form-group">
        <label>Company Registration No <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="reg_no" name="reg_no" required>
      </div>   
 
       <div class="form-group">
        <label>Nature of Business</label>
        <input type="text" class="form-control" id="buisness_nature" name="buisness_nature">
      </div>     
    
         <div class="form-group">
        <label>Address Line 1 <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="address_0" name="address_0" required>
      </div>   
 
       <div class="form-group">
        <label>Town / City <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="town" name="town" required>
      </div> 
    
           <div class="form-group">
        <label>Postcode / ZIP <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="postcode" name="postcode" required>
      </div>   
 

    
  </div><!-- col_l -->
  
    <div class="col-sm-6 form_col form-col_r">

             <div class="form-group">
        <label>Trading Name</label>
        <input type="text" class="form-control" id="trading_name" name="trading_name">
      </div> 
      
             <div class="form-group">
        <label>Surname <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="surname" name="surname" required>
      </div> 
      
             <div class="form-group">
        <label>Website Address <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="web_1" name="web_1">
      </div> 
      
             <div class="form-group">
        <label>Your Position in Company <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="position" name="position" required>
      </div> 
      
             <div class="form-group">
        <label>VAT No (if applicable) <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="vat_no" name="vat_no" required>
      </div> 
      
             <div class="form-group">
        <label>Year of Incorporation</label>
        <input type="text" class="form-control" id="incorporation" name="incorporation">
      </div> 
      
             <div class="form-group">
        <label>Address Line 2</label>
        <input type="text" class="form-control" id="address_1" name="address_1">
      </div> 
      
             <div class="form-group">
        <label>County / State</label>
        <input type="text" class="form-control" id="county" name="county">
      </div> 
      
             <div class="form-group">
        <label>Country <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="country" name="country" required>
      </div> 
  </div><!-- col_r -->
  
   <div class="clear" style="height:0px;"></div>
  
  <div class="col-sm-12">
    <h2>
      Trade Reference
    </h2>
  </div>
  
     <div class="clear" style="height:10px;"></div>
  
      <div class="col-sm-6 form_col form-col_l">

             <div class="form-group">
        <label>Company Name <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="company_2" name="company_2" required>
      </div> 
      
             <div class="form-group">
        <label>Contact Name <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="nane_2" name="nane_2" required>
      </div> 
      
             <div class="form-group">
        <label>Telephone Number <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="phone_2" name="phone_2" required>
      </div> 
      
  </div><!-- col_l -->
  
      <div class="col-sm-6 form_col form-col_r">

             <div class="form-group">
        <label>Website Address</label>
        <input type="text" class="form-control" id="web_2" name="web_2">
      </div> 
      
             <div class="form-group">
        <label>Email Address <span style="color:red;">*</span></label>
        <input type="text" class="form-control" id="email_2" name="email_2" required>
      </div> 
      
             <div class="form-group">
        <label>Account No / Ref</label>
        <input type="text" class="form-control" id="account_ref" name="account_ref">
      </div> 
 
  </div><!-- col_r -->
  
  <div class="col-sm-12 form_col form-col_l">
               <div class="form-group">
        <label>Address <span style="color:red;">*</span></label>
                 <textarea class="form-control" id="address" name="address" required rows="5"></textarea>
      </div>
    
    <div class="clear" style="height:10px;"></div>
    <div class="form-group">     
<div class="checkbox">
<label><input type="checkbox" value="yes" id="news" name="news"><span style="font-size:12px;position:relative;top:12px;">
  Tick the box if you wish to be contacted about our other Ventura products and services.
  </span></label>
</div>
</div>
    
 <div class="clear" style="height:20px;"></div>
    
 <div class="form-group">
<button type="submit" class="btn btn-primary">SUBMIT</button>
</div>
    
    
  </div><!-- col_l -->
  
  
  
  <div class="clear" style="height:0px;"></div>








</form>
</div><!-- hospitality_form_box -->
<div class="clear" style="height:40px;"></div>

    <!--
<p  style="text-align:center;">
  Note: The winner will be picked by random draw by Friday 9th Feb. No cash value.
</p>
-->

<div class="clear" style="height:40px;"></div>


<div class="modal fade" tabindex="-1" role="dialog" id="pw_sending_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>Sending message ...</p>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="clear" style="height:40px;"></div>

<?php endwhile; ?>
<?php else : ?>

		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>
 
<?php endif; ?>
    
    </div><!-- hospitality_container -->
   <div class="clear" style="height:0px;"></div>
</div> 
  
  <div class="clear" style="height:0px;"></div>


<?php get_footer(); ?>


<script>
    $(document).ready(function () {
			
			/// $('.screen_one_container').css('min-height', ( $(window).height() -100 ) + 'px');
      
        $("#pw_contact_trade").submit(function () {
					
					          $('#pw_sending_modal').modal({
          //    backdrop: 'static',
         //     keyboard: false
            });
   
          var form_data = $("#pw_contact_trade").serialize();
          trade_sendData(form_data);
       
            
            return false; // ne change pas de page
        });
      
     
      
      
      function trade_sendData(form_data){
   
        $.ajax({
        url: wpApiSettings.root + 'pw/trade_email/',
        method: 'POST',
        data: form_data,
        crossDomain: true,
        beforeSend: function ( xhr ) {
           xhr.setRequestHeader( 'X-WP-Nonce', wpApiSettings.nonce );
        },
        success: function( data ) {
            console.log('success');
            console.log(data);
      
            //var obj_data = JSON.parse(data);
  var texte = data.code;
                 $('#alert').show();
                if (texte == "success") {
                    window.location = "<?php echo site_url(); ?>/thank-you-trade/";
						//			goog_report_conversion('http://www.conradjones.com/thank-you/')
                 document.getElementById('zoneSub').innerHTML = '<div class="alert alert-success" role="alert">Thank you for your message.</div>';

                    $('#pw_contact_form_hosp').html('<div class="clear" style="height:100px;padding-top:40px;"></div>');
									 $('#pw_sending_modal').modal('hide');
                }
                else if (texte == "email_error") {
                    document.getElementById('zoneSub').innerHTML = '<div class="alert alert-danger" role="alert">Sorry. Message cannot be sent at this time.</div>';
                }
                else if (texte == "invalid") {
                    document.getElementById('zoneSub').innerHTML = '<div class="alert alert-danger" role="alert">This email is invalid</div>';
                }         
          
          
        }
    });
    
    
    
  } //sendData  
      
      

    });

</script>

<!--
files: page-hospitality, fc/hospitality


company
firstname
email_1
phone_1
reg_no
buisness_nature
address_0
town
postcode
trading_name
surname
web_1
position
vat_no
incorporation
address_1
county
country


company_2
nane_2
phone_2
web_2
email_2
account_ref
address


-->

</body>
</html>