<?php get_header(); ?>



<div class="clear" style="height:0px"></div>


<div class="container single_post_container">



<?php if (have_posts()) : ?>
 <?php while (have_posts()) : the_post(); ?>



<h2 class="single_post_title">
  <?php the_title() ?>
  </h2>




<?php the_content(); ?>

<div class="clear" style="height:10px;"></div>



<?php endwhile; ?>
<?php else : ?>

		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>
 
<?php endif; ?>



</div><!-- single_post_container -->

<div class="clear" style="height:60px;"></div>


<?php get_footer(); ?>


</body>
</html>