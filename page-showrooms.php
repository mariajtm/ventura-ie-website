<?php

/**
* Template Name: New Showrooms Page
* Selectable from a dropdown menu on the edit page screen. All images will be in lightbox
*/
?>


<?php get_header(); ?>


<style>
  @media screen and (max-width: 780px) {
    .portfolio_nav{
      min-height:80px;
    }
    .mobile_clr{
      height:20px;
    }
  }
  
</style>



<div class="clear" style="height:0px"></div>

<div class="container portfolio_container">

<div class="col-sm-12 tagline" paw-on-mobile="false" paw-animate="fadeInDown" paw-delay="500">
    <h1>Our Showrooms</h1>
    <span style="text-align:center;">
      <?php the_content(); ?>   
    </span>
</div>
<div class="clear" style="height:0px"></div>



<div class="col-sm-12 portfolio_holder">


<?php
$mainUrl = get_template_directory_uri() . '/';


$type = 'showroom';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
);

$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post();
  $format = get_post_format( $post->ID );
  
  if($format == 'video'){
    $desc = get_post_meta( $post->ID, 'pw_portfolio_video_box_desc', true );
    $video = get_post_meta( $post->ID, 'pw_portfolio_video_video', true );
    $poster= get_post_meta( $post->ID, 'pw_portfolio_video_image', true );
    include('partials/loop_video.php');
  }else{
    $desc = get_post_meta( $post->ID, 'pw_portfolio_gallery_box_desc', true );
    $poster= get_post_meta( $post->ID, 'pw_portfolio_gallery_image', true );
    $content = get_the_content();

    include('partials/loop_gallery_new.php');
  }

endwhile;
}
?>



</div><!-- portfolio_holder -->





</div><!-- portfolio_container -->


<div class="clear" style="height:120px"></div>









<?php get_footer(); ?>










</body>
</html>


