<?php

/*
$singular_name = 'Sample';
$pluar_name = 'Samples';
$post_slug = 'sample';
$supports_array = array('title','editor','author','thumbnail','excerpt','comments','custom-fields');
$menu_icon = get_bloginfo("template_url") .'/img/icon.png'; //or 'dashicons-lightbulb'
$show_in_rest = true //false
*/

function generate_arags($singular_name, $pluar_name, $post_slug, $supports_array, $menu_icon, $show_in_rest, $publicly_queryable = true){
	
		$slides_labels = array(
			'name' => _x( $pluar_name, 'post type general name'),
			'singular_name' => _x( $singular_name, 'post type singular name'),
			'all_items' => __('All '. $pluar_name),
			'add_new' => _x('Add new '. $singular_name, 'sliders'),
			'add_new_item' => __('Add new '. $singular_name),
			'edit_item' => __('Edit ' .$singular_name),
			'new_item' => __('New ' . $singular_name),
			'view_item' => __('View '. $singular_name),
			'search_items' => __('Search in Questions'),
			'not_found' =>  __('No '.$pluar_name.' found'),
			'not_found_in_trash' => __('No '.$pluar_name.' found in trash'),
      'parent_item_colon' => ''
      
		);
		$args = array(
			'labels' => $slides_labels,
			'public' => true,
			'publicly_queryable' => $publicly_queryable,
			//'menu_icon' => get_bloginfo("template_url") .'/img/icon.png',
		'menu_icon' => $menu_icon,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => 5,
		'show_in_rest'       => $show_in_rest,
		  'rest_base'          => $post_slug,
		  'rest_controller_class' => 'WP_REST_Posts_Controller',
			//'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
			'supports' => $supports_array,
			'has_archive' => $post_slug
		);
		return $args;
	}



// create portfolio custom post type
add_action('init', 'portfolio_post_type_init');
function portfolio_post_type_init(){
    $singular_name = 'Portfolio';
    $pluar_name = 'Portfolios';
    $post_slug = 'portfolio';
    $supports_array = array('title','thumbnail','editor', 'post-formats');
    $menu_icon = 'dashicons-album'; 
    $show_in_rest = true;
	$publicly_queryable = true;
   $args = generate_arags($singular_name, $pluar_name, $post_slug, $supports_array, $menu_icon, $show_in_rest, $publicly_queryable );
   register_post_type('portfolio',$args);
}


//custom taxonomy "realizations"
add_action( 'init', 'create_realizations_hierarchical_taxonomy', 0 );

function create_realizations_hierarchical_taxonomy() {
  $pw_name_singular = 'Realization';
  $pw_name_pluar = 'Realizations';
  $labels = array(
    'name' => _x( $pw_name_pluar, 'taxonomy general name' ),
    'singular_name' => _x( $pw_name_singular, 'taxonomy singular name' ),
    'search_items' =>  __( 'Search ' . $pw_name_pluar ),
    'all_items' => __( 'All '. $pw_name_pluar ),
    'parent_item' => __( 'Parent '. $pw_name_singular ),
    'parent_item_colon' => __( 'Parent '.$pw_name_singular.':' ),
    'edit_item' => __( 'Edit '.$pw_name_singular ), 
    'update_item' => __( 'Update '.$pw_name_singular ),
    'add_new_item' => __( 'Add New '.$pw_name_singular ),
    'new_item_name' => __( 'New '.$pw_name_singular.' Name' ),
    'menu_name' => __( $pw_name_pluar ),
  ); 	

  register_taxonomy('realizations',array('portfolio'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'realizations' ),
  ));

}



// create product custom post type
add_action('init', 'product_post_type_init');
function product_post_type_init(){
    $singular_name = 'Product';
    $pluar_name = 'Products';
    $post_slug = 'product';
    $supports_array = array('title','thumbnail','editor','page-attributes');
    $menu_icon = 'dashicons-cart'; 
    $show_in_rest = true;
   $args = generate_arags($singular_name, $pluar_name, $post_slug, $supports_array, $menu_icon, $show_in_rest);
   register_post_type('product',$args);
}


//custom taxonomy "sort"
add_action( 'init', 'create_sort_hierarchical_taxonomy', 0 );

function create_sort_hierarchical_taxonomy() {
  $pw_name_singular = 'Type';
  $pw_name_pluar = 'Types';
  $labels = array(
    'name' => _x( $pw_name_pluar, 'taxonomy general name' ),
    'singular_name' => _x( $pw_name_singular, 'taxonomy singular name' ),
    'search_items' =>  __( 'Search ' . $pw_name_pluar ),
    'all_items' => __( 'All '. $pw_name_pluar ),
    'parent_item' => __( 'Parent '. $pw_name_singular ),
    'parent_item_colon' => __( 'Parent '.$pw_name_singular.':' ),
    'edit_item' => __( 'Edit '.$pw_name_singular ), 
    'update_item' => __( 'Update '.$pw_name_singular ),
    'add_new_item' => __( 'Add New '.$pw_name_singular ),
    'new_item_name' => __( 'New '.$pw_name_singular.' Name' ),
    'menu_name' => __( $pw_name_pluar ),
  ); 	

  register_taxonomy('sort',array('product'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'sort' ),
  ));

}





//custom taxonomy "Country"
add_action( 'init', 'create_country_hierarchical_taxonomy', 0 );

function create_country_hierarchical_taxonomy() {
  $pw_name_singular = 'Country';
  $pw_name_pluar = 'Countries';
  $labels = array(
    'name' => _x( $pw_name_pluar, 'taxonomy general name' ),
    'singular_name' => _x( $pw_name_singular, 'taxonomy singular name' ),
    'search_items' =>  __( 'Search ' . $pw_name_pluar ),
    'all_items' => __( 'All '. $pw_name_pluar ),
    'parent_item' => __( 'Parent '. $pw_name_singular ),
    'parent_item_colon' => __( 'Parent '.$pw_name_singular.':' ),
    'edit_item' => __( 'Edit '.$pw_name_singular ), 
    'update_item' => __( 'Update '.$pw_name_singular ),
    'add_new_item' => __( 'Add New '.$pw_name_singular ),
    'new_item_name' => __( 'New '.$pw_name_singular.' Name' ),
    'menu_name' => __( $pw_name_pluar ),
  ); 	

  register_taxonomy('country',array('product'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'country' ),
  ));

}//create_country_hierarchical_taxonomy








// create portfolio custom post type
add_action('init', 'showroom_post_type_init');
function showroom_post_type_init(){
    $singular_name = 'Showroom';
    $pluar_name = 'Showrooms';
    $post_slug = 'showroom';
    $supports_array = array('title','thumbnail','editor', 'post-formats');
    $menu_icon = 'dashicons-store'; 
    $show_in_rest = true;
	$publicly_queryable = true;
   $args = generate_arags($singular_name, $pluar_name, $post_slug, $supports_array, $menu_icon, $show_in_rest, $publicly_queryable );
   register_post_type('showroom',$args);
}




