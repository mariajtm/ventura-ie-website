<?php get_header(); ?>

<style>
  .product_list_item_product_info h5{
    font-family: 'Century gothic', serif;
    margin: 0;
    
  }
 .icon-prev:before, .icon-next:before{
  font-size:80px;
} 
#carouselExampleIndicators .item img{
    object-fit: contain;
    height: 80vh;
    width: 100%;
}
@media (max-width:991px) {
  .icon-prev:before, .icon-next:before {
    display: none;
  }
  
}
</style>


<div class="clear" style="height:150px"></div>

<div class="container">

  
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $mainDesc= get_post_meta( $post->ID, 'mt_portfolio_main_desc', true );?>

 <div class="col-sm-12 text-center" paw-on-mobile="false" paw-animate="fadeInUp" paw-delay="0">
   <h2><?php echo get_the_title() ?></h2>
   <div class="clear" style="height:20px;"></div>

   <p class="portfolio_descr"><?php echo $mainDesc?></p>
  </div> 
  
   <div class="clear" style="height:0px;"></div>
  <div class="row align-items-center">
  <div class="col-sm-12">
    
 <?php
    
    $poster= get_post_meta( $post->ID, 'pw_portfolio_video_image', true );
    $packages = get_post_meta( $post->ID, 'pw_portfolio_products_related_products', true );
    $gallery_poster = get_post_meta( $post->ID, 'pw_portfolio_gallery_image', true );
    
    $content = get_the_content();
     

  ?>
  <?php
$htmlContent = get_the_content();
$origImageSrc = array();

preg_match_all('/<img[^>]+>/i',$htmlContent, $imgTags); 

for ($i = 0; $i < count($imgTags[0]); $i++) {
  preg_match('/src="([^"]+)/i',$imgTags[0][$i], $imgage);
  // remove opening 'src=' tag, can`t get the regex right
  $origImageSrc[] = str_ireplace( 'src="', '',  $imgage[0]);
}

//print_r($origImageSrc);

?>



  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
  <ol class="carousel-indicators">
  <?php
      $key=0;
    foreach( $origImageSrc as $key=>$image){
        if($key == 0){$class = 'active';
        }else{
            $class = ' ';
        }
        echo 
        '<li data-target="#carouselExampleIndicators" data-slide-to="'.$key.'" class="'.$class.'"></li>';
    }?>
  </ol>

  <!-- Content -->
  <div class="carousel-inner text-center"  role="listbox">
    <!-- Slides -->

      <?php
      $key=0;
    foreach( $origImageSrc as $key=>$image){
        if($key == 0){$class = 'active';
        }else{
            $class = ' ';
        }
        echo ' 
        
        <div class="item  '.$class.'" >
          <img src="'. $image .'" class="img-fluid">
        </div>';
    }?>
  </div>
  <a class="left carousel-control" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="icon-prev" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="icon-next" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  
    
</div><!-- portfolio_holder -->

</div>
  
<div class="clear" style="height:40px;"></div>
<?php if($packages != 0){
  echo'<div class="col-sm-12 text-center">
  <h3>ITEMS IN THIS ROOM</h3> 
</div>';
}?>
  <div class="col-sm-12 single_portfolio_content" style="background-color: #dadada;">
    

  <?php 
  foreach( $packages as $cons){

  ?>
    
    <a href="<?php echo $cons['link']?>" >
      
        <div class="col-xs-6 col-md-4 text-center" style="padding:10px;" paw-on-mobile="false"paw-animate="zoomIn" paw-delay="300" style="margin-bottom:5px;">
              <img src="<?php echo $cons['image'];?>" alt="">
                  <div class="product_list_item_product_info" style="background-color: #fff;padding-bottom:20px">
                    
                          <h5 class="text-uppercase"><?php echo $cons['name'];?></h5>
                  </div>
          </div>
    
    </a>
  <?php } ?>

  </div>

<div class="clear" style="height:80px;"></div>



<?php endwhile; ?>
<?php else : ?>
  <div class="container single_portfolio_content">
		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>
  </div>
<?php endif; ?>

</div><!-- portfolio_container -->


<div class="clear" style="height:10px;"></div>


<?php get_footer(); ?>
<script>
  $('.carousel').carousel({
  interval: false,
});
</script>

</body>
</html>