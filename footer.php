<!-- Pinterest Modal -->
<div class="modal fade" id="pw_pinterestModal" tabindex="-1" role="dialog" style="z-index: 99999;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;padding-top:20%;">
        <!--
        <a id="pw_pinterest_link" class="btn btn-primary" href="#" target="_blank">Shere on <i class="fa fa-pinterest" aria-hidden="true"></i> Pinterest</a>
-->
       
        <a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" data-pin-custom="true" class="btn btn-primary" style="width:200px;max-width:100%;">
          Share on <i class="fa fa-pinterest" aria-hidden="true"></i> Pinterest
        </a>
        <div class="clear" style="height:10px;"></div>
         <button type="button" class="btn btn-primary" data-dismiss="modal" style="width:200px;max-width:100%;">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- // Pinterest Modal -->





<!-- Modal -->
<div class="modal modal-wide modal_wide_video fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal_content_video">
      <div class="modal-body">
          <div class="video_close_btn" data-dismiss="modal">&times;</div>
          <div class="clear" style="height:0px"></div>
       <div class="col-sm-12 video_iframe"></div>
       <div class="clear" style="height:0px"></div>
       
      </div>
    </div>
  </div>
</div>




<div class="clear" style="height:0px;"></div>

<?php
  global $post;
  $options = get_option('wp78points_theme_options');

  $themeLink_1 = esc_attr( get_option('themeLink_1'));
  $themeLink_2 = esc_attr( get_option('themeLink_2'));
  $themeLink_3 = esc_attr( get_option('themeLink_3'));
  $themeLink_4 = esc_attr( get_option('themeLink_4'));
  $themeLink_5 = esc_attr( get_option('themeLink_5'));
  $themeLink_6 = esc_attr( get_option('themeLink_6'));
  $themeLink_7 = esc_attr( get_option('themeLink_7'));
?>
  
<?php
    $direct_parent_id = $post->post_parent;
    $direct_parent = get_post($direct_parent_id); 
    $direct_parent_slug = $direct_parent->post_name;
?>

<style>
@media only screen and (max-width: 760px) {
  .footer-col_iner_l_widget_text {
    padding-bottom:50px;
  }
}
</style>

<div id="footer" class="footer col-sm-12">

  <div class="container footer_container p-2">
  <div class="clear" style="height:20px;"></div>

    <div class="row">
      <div class="col-xm-12 col-sm-10">


        <div class="col-xs-12 col-sm-3 footer_col footer_col_l ">

          <p style="font-weight: 400;color: #fff;font-size: 14px;">Ventura's World<p>
          <a href="<?php echo home_url();?>#who-we-are">About us</a> <br>
          <a href="<?php echo home_url();?>/workshop">Our Workshop</a> <br>
          <a href="<?php echo home_url();?>/gallery">Our Inspiration</a> <br>

        </div>
        <div class="col-xs-12 col-sm-3 footer_col footer_col_l">

          <p style="font-weight: 400;color: #fff;font-size: 14px;">Your order<p>
          <a href="https://shop.ventura.ie/pages/returns-policy">Delivery & returns</a> <br>
          <!-- <a href="#">Care guides</a> <br>-->
          <a href="https://shop.ventura.ie/products/gift-card">Gift Cards</a> <br>
          <!-- <a href="#">FAQs</a> <br>  -->

        </div>
        <div class="col-xs-12 col-sm-3 footer_col footer_col_l ">

          <p style="font-weight: 400;color: #fff;font-size: 14px;">Policies<p>
          <a href="http://ventura.ie/terms/">Terms & conditions</a> <br>
          <a href="http://ventura.ie/cookie-policy/">Cookie Policy</a> <br>
          <a href="http://ventura.ie/privacy-policy-updated/">Privacy Policy</a> <br>
          <a href="https://ventura.ie/venturas-commitment-to-environmentally-focused-design/">Ventura Commitment</a> <br>
          <!-- <a href="https://ventura.ie/child-labour-prevention/"> Child Labour Prevention Statement</a> <br>
          <a href="https://ventura.ie/modern-slavery-prevention/"> Modern Slavery Policy Statement</a> <br> -->

        </div>
        <div class="col-xs-12 col-sm-3 footer_col footer_col_l ">

          <p style="font-weight: 400;color: #fff;font-size: 14px;">Stay in Touch<p>
          <a href="http://ventura.ie/contact/">Contact Us</a> <br>
          <a href="https://ventura.ie/all-showrooms/">Visit our Showrooms</a> <br>
          <a href="https://ventura.ie/appointments/">Book an appointment</a> <br>

        </div>
      </div>
      <div class="col-xs-12 col-sm-2 footer_col footer_col_l">

        <p style="color:#fff;font-family: 'Muli', sans-serif;font-weight: 400;color: #fff;font-size: 14px;">
          Media Enquiries
        </p>
        <p style="color:#fff;font-size: 14px;">
          For press or media enquiries for Ventura Design or to enquire about our podcast, Shut the Front Door, please contact our PR consultant <a href="mailto:grace@ventura.ie" style="color:#fff;font-weight:bold;">grace@ventura.ie</a>
        </p>

      </div>
    </div>
    <div class="clear" style="height:20px;"></div>

    <div class="row d-flex">
      <div class="col-sm-12 col-md-6 credit">
        <a href="http://ventura.ie"> V E N T U R A © Copyright 2022 </a>
      </div>
      <div class="col-sm-12 col-md-6" style="text-align:end">
    
        <span class="fo f_1"><a href="https://www.pinterest.ie/ventura_design/" target="_blank" style="font-size: 20px"><i class="fa fa-pinterest-square"></i></a></span>
      
        
        <span class="fo f_1"><a href="https://www.linkedin.com/company/2853574/" target="_blank" style="font-size: 20px"><i class="fa fa-linkedin"></i></a></span>
        
        <span class="fo f_1"><a href="https://www.facebook.com/VenturaInteriorDesign" target="_blank" style="font-size: 20px"><i class="fa fa-facebook-official"></i></a></span>
      

        


        <span class="fo f_1"><a href="https://www.instagram.com/venturainteriordesign/" target="_blank" style="font-size: 20px"><i class="fa fa-instagram"></i></a></span>
        



        <span class="fo f_1"><a href="https://www.houzz.ie/pro/arlenemcintyre1/ventura-design" target="_blank" style="font-size: 20px"><i class="fa fa-houzz"></i></a></span>
        
        <span class="fo f_1"><a href="https://www.youtube.com/channel/UC98FEZDJy2EhvSqV_pzaDbg" target="_blank" style="font-size: 20px"><i class="fa fa-youtube"></i></a></span>
        
      
        
        <span class="fo f_1"><a href="https://vimeo.com/user71091242" target="_blank" style="font-size: 20px"><i class="fa fa-vimeo"></i></a></span>
        
        
        
        


        
      </div>

    </div>



    <div class="clear" style="height:40px;"></div> 
        
  </div><!-- footer_container -->

</div><!-- footer -->

<div class="clear" style="height:0px;"></div>


<div class="modal fade" tabindex="-1" role="dialog" id="ventura_newsletter_popup">
  <div class="modal-dialog modal-sm" role="document" style="background-color:#fff;margin-top:15%;">
    <div class="modal-content">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="col-sm-12">
        <div class="clear" style="height:0px;"></div>
        <div class="response" id="mce-error-response" style="display:none;color: #000;"></div>
		    <div class="response" id="mce-success-response" style="display:none;color: #000;"></div>
        
<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<div id="mc_embed_signup">
<form action="https://ventura.us2.list-manage.com/subscribe/post?u=0e083ebf89336ac206fe34718&amp;id=78efe1cd04" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
      <h2>Join The World of Ventura Mailing List!</h2> 
    <p style="font-weight: 300;">
      Receive our exquisite interior inspirations, interior trends, luxury living ideas, our recent projects, and much more.
      </p>
<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0e083ebf89336ac206fe34718_78efe1cd04" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
 <div class="clear" style="height:20px;"></div>       
    <!-- 
     <form id="mc-embedded-subscribe-form">
        <div class="col-sm-12">
          
<div id="alert">
   <div id="zoneSubMailchimp" class="error"></div>
 </div>
          
          <div class="clear" style="height:0px;"></div>
          <p style="font-weight:400;text-align:center;">
            Join our mailing list!
          </p>
          <div class="clear" style="height:0px;"></div>
            <div class="form-group">
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required style="border-radius:0px;">
            </div>
         
           <div class="clear" style="height:0px;"></div>
           <button type="submit" class="btn btn-primary" style="width:100%;">Sign in</button>
        </div>
        </form>
-->
      </div>
        <div class="clear" style="height:20px;"></div>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="clear" style="height:0px;"></div>


<!-- <a href="#" class="scrollToTop"></a> -->
<div id="main_cover" class="cover"></div> 

<!-- <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js?ver=1.0.0"></script> -->
<script src="https://venturastatic.s3-eu-west-1.amazonaws.com/js/jquery.min.js?ver=1.0.0"></script>



<script src="https://venturastatic.s3-eu-west-1.amazonaws.com/assets/bootstrap/js/bootstrap.min.js?ver=1.0.0"></script>
<script src="https://venturastatic.s3-eu-west-1.amazonaws.com/js/jquery.waypoints.min.js?ver=1.0.0"></script>
<script src="https://venturastatic.s3-eu-west-1.amazonaws.com/js/inview.min.js?ver=1.0.0"></script>
<script src="https://venturastatic.s3-eu-west-1.amazonaws.com/js/jquery.easing.min.js?ver=1.0.0"></script>
<script src="https://venturastatic.s3-eu-west-1.amazonaws.com/js/match_height.js?ver=1.0.0"></script>
<script src="https://venturastatic.s3-eu-west-1.amazonaws.com/js/vide.min.js?ver=1.0.0"></script>
<!-- <script src="https://venturastatic.s3-eu-west-1.amazonaws.com/js/custom.js?ver=1.0.0"></script> -->
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js?ver=1.0.1"></script>



   <?php // echo do_shortcode("[greet_me]"); ?>
    <?php if(do_shortcode("[greet_me]") == 'true'){ ?>
      <!-- display popup -->
      <script>
        $( document ).ready(function() {
          setTimeout(
          function() {
            console.log('email prompt');
            $('#ventura_newsletter_popup').modal();
          }, 2000);
        }); //ready
      </script>
     <?php }?>


<script>
$( document ).ready(function() {
  /*
  $('#main_video').vide({
  mp4: 'video_2/video_new.mp4',
  webm: 'video_2/video_new.webm',
  ogv: 'video_2/video_new.ogv',
  poster: 'video_2/th_277.jpg',
},
{
  volume: 0,
  muted: true,
  loop: true,
  autoplay: true,
  position: '50% 50%',
  posterType: 'jpg',
}
);
  */
}); //ready
</script>

 <script src="<?php echo get_template_directory_uri(); ?>/js/video_data.js?ver=1.0.1"></script>
<script>
// $( document ).ready(function() {
 
//   $('#main_video').vide({
// //  mp4: '<?php echo get_site_url(); ?>/video_5/video.mp4', 
//   mp4: 'https://venturastatic.s3-eu-west-1.amazonaws.com/video.mp4?v=1.0', 
//  // webm: '<?php echo get_site_url(); ?>/video_5/video.webm', 
//   webm: 'https://venturastatic.s3-eu-west-1.amazonaws.com/video.webm?v=1.0',
// //  ogv: '<?php echo get_site_url(); ?>/video_5/video.ogv', 
//   ogv: 'https://venturastatic.s3-eu-west-1.amazonaws.com/video.ogv?v=1.0',
// //  poster: '<?php echo get_site_url(); ?>/video_5/poster.jpg', 
//     poster: 'https://venturastatic.s3-eu-west-1.amazonaws.com/video.jpg?v=1.0',
// },
// {
//   volume: 0,
//   muted: true,
//   loop: true,
//   autoplay: true,
//   position: '50% 50%',
//   posterType: 'jpg',
// }
// );
 
  
// }); //ready
</script>

 <script src="https://venturastatic.s3-eu-west-1.amazonaws.com/assets/fancy3/jquery.fancybox.min.js?ver=1.0.0"></script>
    
    <script type="text/javascript">
        $("[data-fancybox]").fancybox({
            loop : false,
            margin : [0, 0],
           // toolbar : false,
           buttons : [
      //  'slideShow',
       // 'fullScreen',
       // 'thumbs',
        //'download',
        'pwpinterest',
       // 'share',
        'close'
    ],
        });
      
      
    
 $.fancybox.defaults.btnTpl.pwpinterest = '<button data-fancybox-pwpinterest class="fancybox-button fancybox-button-pwpinterest" title="Pinterest">' +
        '<img src="<?php echo get_template_directory_uri(); ?>/img/pin.png" />' +
    '</button>';  
      
 
      /*
 $.fancybox.defaults.btnTpl.pwpinterest = '<a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" data-pin-custom="true" class="fancybox-button fancybox-button-pwpinterest" title="Pinterest"><img src="<?php echo get_template_directory_uri(); ?>/img/pin.png" /></a>';      
   */   
      
      
 //<a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" data-pin-custom="true" class="btn btn-primary">Shere on <i class="fa fa-pinterest" aria-hidden="true"></i> Pinterest</a>     
      
      
   $('body').on('click', '[data-fancybox-pwpinterest]', function() {
    var f = $.fancybox.getInstance();

    if ( f ) {
        console.log(f.current.src);
    //  document.getElementById("pw_pinterest_link").href= 'https://www.pinterest.ie/pin/find/?description=Ventura Design&url=' + f.current.src; 
      $('#pw_pinterestModal').modal('toggle')
    }
});   
      
      
    </script>






<?php wp_footer() ?>

